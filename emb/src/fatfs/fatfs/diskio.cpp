/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h" /* FatFs lower layer API */
#include <hw/SdcardDriver.h>

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize(BYTE pdrv)
{
    auto& sd = hw::sdcard::SdcardDriver::Instance();

    if (sd.is_initialized and (sd.detect() == hw::sdcard::SD_PRESENT)) {
        return RES_OK;
    }

    return RES_ERROR;
}

/*-----------------------------------------------------------------------*/
/* Return Disk Status                                                    */
/*-----------------------------------------------------------------------*/
DSTATUS disk_status(BYTE pdrv)
{
    if (pdrv)
        return STA_NOINIT;
    return 0;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/
DRESULT disk_read(BYTE pdrv,    /* Physical drive nmuber (0..) */
                  BYTE* buff,   /* Data buffer to store read data */
                  DWORD sector, /* Sector address (LBA) */
                  BYTE count    /* Number of sectors to read (1..128) */
)
{
    auto& sd = hw::sdcard::SdcardDriver::Instance();

    if (sd.read_blocks((uint8_t*)buff, sector << 9, count) == hw::sdcard::OpResult::OK) {
        if (sd.wait_read_operation() == hw::sdcard::OpResult::OK) {
            while (sd.get_status() != hw::sdcard::TransferState::OK)
                ;
            return RES_OK;
        }
    }

    return RES_ERROR;
}

#if !_READONLY
DRESULT disk_write(BYTE pdrv,        /* Physical drive nmuber (0..) */
                   const BYTE* buff, /* Data to be written */
                   DWORD sector,     /* Sector address (LBA) */
                   BYTE count        /* Number of sectors to write (1..128) */
)
{
    auto& sd = hw::sdcard::SdcardDriver::Instance();

    if (sd.write_blocks((uint8_t*)buff, sector << 9, count) == hw::sdcard::OpResult::OK) {
        if (sd.wait_write_operation() == hw::sdcard::OpResult::OK) {
            while (sd.get_status() != hw::sdcard::TransferState::OK)
                ;
            return RES_OK;
        }
    }

    return RES_ERROR;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(BYTE pdrv, BYTE cmd, void* buff)
{
    if (pdrv != 0)
        return (RES_PARERR);

    switch (cmd) {
        case CTRL_SYNC:
            // do nothing. By calling SD_WaitReadOperation and
            // SD_WaitWriteOperation we already ensure that operations
            // complete in the read and write functions.
            return (RES_OK);
            break;
        default:
            return (RES_PARERR);
    }
}
#endif

DWORD get_fattime(void)
{
    return ((DWORD)(2013 - 1980) << 25) /* Year 2013 */
           | ((DWORD)7 << 21)           /* Month 7 */
           | ((DWORD)28 << 16)          /* Mday 28 */
           | ((DWORD)0 << 11)           /* Hour 0 */
           | ((DWORD)0 << 5)            /* Min 0 */
           | ((DWORD)0 >> 1);           /* Sec 0 */
}
