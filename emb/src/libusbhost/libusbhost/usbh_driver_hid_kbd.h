#ifndef USBH_DRIVER_HID_KBD
#define USBH_DRIVER_HID_KBD

#include "usbh_core.h"
#include <stdint.h>

BEGIN_DECLS

typedef void (*t_read_kbd)(void* data, uint8_t data_len);

struct _hid_kbd_config {
	void (*kbd_in_message_handler)(uint8_t device_id, const uint8_t *data , uint32_t length);
};
typedef struct _hid_kbd_config hid_kbd_config_t;

void hid_kbd_driver_init(const hid_kbd_config_t *config);

extern const usbh_dev_driver_t usbh_hid_kbd_driver;


typedef struct _hid_mouse_config hid_config_t;


/**
 * @brief hid_set_report
 * @param device_id handle of HID device
 * @returns true on success, false otherwise
 */
bool hid_set_report(uint8_t device_id, uint8_t val);
bool is_hid_ready_for_set_report(uint8_t device_id);

enum HID_TYPE {
	HID_TYPE_NONE,
	HID_TYPE_MOUSE,
	HID_TYPE_KEYBOARD,
};

/**
 * @brief hid_get_type
 * @param device_id handle of HID device
 * @return type of attached HID
 * @see enum HID_TYPE
 */
enum HID_TYPE hid_get_type(uint8_t device_id);

/**
 * @brief hid_is_connected
 * @param device_id handle of HID device
 * @return true if the device with device_id is connected
 */
bool hid_is_connected(uint8_t device_id);


END_DECLS

#endif
