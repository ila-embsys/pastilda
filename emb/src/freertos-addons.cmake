add_library(freertos_addons INTERFACE)

target_sources(freertos_addons INTERFACE
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/ccondition_variable.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cevent_groups.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cmem_pool.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cmutex.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cqueue.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cread_write_lock.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/csemaphore.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/ctasklet.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cthread.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/ctickhook.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/ctimer.cpp
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/cworkqueue.cpp
)

target_include_directories(freertos_addons INTERFACE
	${CMAKE_CURRENT_LIST_DIR}/freertos-addons/c++/Source/include
)

target_compile_definitions(freertos_addons INTERFACE
	CPP_FREERTOS_NO_CPP_STRINGS
	CPP_FREERTOS_NO_EXCEPTIONS
)