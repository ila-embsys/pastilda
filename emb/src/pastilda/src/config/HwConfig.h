#ifndef CONFIG_HWCONFIG_H_
#define CONFIG_HWCONFIG_H_

#include <cm3cpp/gpio.hpp>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/f4/nvic.h>
#include <libopencm3/stm32/flash.h>

namespace hw {

namespace config {

using Gpio = cm3cpp::gpio::Gpio;
using Pinout = cm3cpp::gpio::Gpio::Pinout;

// 1. RCC clock
constexpr rcc_clock_scale RCC_HSE_25MHZ_TO_HCLK_168MHZ{
  25,                                                         // PLLM
  336,                                                        // PLLN,
  2,                                                          // PLLP
  7,                                                          // PLLQ
  1,                                                          // PLLR
  RCC_CFGR_PLLSRC_HSE_CLK,                                    // CLOCK SRC
  (FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_LATENCY_5WS),  // FLASH CONFIG
  RCC_CFGR_HPRE_DIV_NONE,                                     // HPRE
  RCC_CFGR_PPRE_DIV_4,                                        // PPRE1
  RCC_CFGR_PPRE_DIV_2,                                        // PPRE2
  PWR_SCALE1,                                                 // POWER SAVE
  168000000,                                                  // AHB FREQ
  42000000,                                                   // APB1_FREQ
  84000000                                                    // APB2_FREQ
};

constexpr uint32_t SYSTEM_CORE_CLOCK = 168000000;
constexpr uint32_t SYSTEM_CORE_CLOCK_DIV = 1000;

// 2. RCC peripherals list for enable (in progress)
constexpr rcc_periph_clken RCC_PERIPHS_FOR_ENABLE[] = {
  rcc_periph_clken::RCC_GPIOA, rcc_periph_clken::RCC_GPIOB,
  rcc_periph_clken::RCC_GPIOC, rcc_periph_clken::RCC_GPIOD,
  rcc_periph_clken::RCC_TIM6,  // USB host polling timer
  rcc_periph_clken::RCC_SDIO,  // SD-card
  rcc_periph_clken::RCC_DMA2,
  rcc_periph_clken::RCC_OTGFS,  // USB device
  rcc_periph_clken::RCC_OTGHS,  // USB host
  rcc_periph_clken::RCC_CRYP,   // Crypto stuff
};

// 3. RCC peripherals list for reset, should contain all periphs from
// RCC_PERIPHS_FOR_ENABLE
constexpr rcc_periph_rst RCC_PERIPHS_FOR_RESET[] = {
  rcc_periph_rst::RST_GPIOA, rcc_periph_rst::RST_GPIOB, rcc_periph_rst::RST_GPIOC,
  rcc_periph_rst::RST_GPIOD, rcc_periph_rst::RST_TIM6,  rcc_periph_rst::RST_SDIO,
  rcc_periph_rst::RST_DMA2,  rcc_periph_rst::RST_OTGFS, rcc_periph_rst::RST_OTGHS,
  rcc_periph_rst::RST_CRYP};

// 4. GPIO stuff
constexpr Pinout LED_RED = PA0;
constexpr Pinout LED_GREEN = PA1;
constexpr Pinout LED_BLUE = PA2;

constexpr Pinout USB_HOST_P = PB15;
constexpr Pinout USB_HOST_M = PB14;
constexpr Gpio::AltFuncNumber USB_HOST_AF = Gpio::AltFuncNumber::AF12;
constexpr uint8_t USB_HOST_TIMER_NUMBER = 6;
constexpr uint16_t USB_HOST_TIMER_PRESCALER = (8400 - 1);
constexpr uint16_t USB_HOST_TIMER_PERIOD = (65535);

constexpr Pinout USB_DEVICE_P = PA11;
constexpr Pinout USB_DEVICE_M = PA12;
constexpr Gpio::AltFuncNumber USB_DEVICE_AF = Gpio::AltFuncNumber::AF10;
constexpr uint8_t USB_DEVICE_IRQ = NVIC_OTG_FS_IRQ;
constexpr uint8_t USB_DEVICE_IRQ_PRIORITY = 0xB0;

namespace sdio {

constexpr uint8_t PIN_COUNT = 6;
constexpr Gpio::AltFuncNumber ALT_FUNC = Gpio::AltFuncNumber::AF12;
constexpr Pinout DATA_PINS[] = {PC8, PC9, PC10, PC11};
constexpr Pinout CK_PIN = PC12;
constexpr Pinout CMD_PIN = PD2;

constexpr Pinout SD_DET_PIN = PC7;

constexpr uint32_t IRQ = NVIC_SDIO_IRQ;
constexpr uint32_t IRQ_PRIORITY = 0x00;

namespace dma {
constexpr uint32_t PERIPH = DMA2;
constexpr uint32_t STREAM = DMA_STREAM3;
constexpr uint32_t CHANNEL = DMA_SxCR_CHSEL_4;
constexpr uint32_t IRQ = NVIC_DMA2_STREAM3_IRQ;
constexpr uint32_t IRQ_PRIORITY = 0x80;
}  // namespace dma

}  // namespace sdio


} /* namespace config */

} /* namespace hw */

#endif /* CONFIG_HWCONFIG_H_ */
