#include "TasksConfig.h"

namespace tasks {

namespace config {

constexpr const char LedBlinkingTask::NAME[];
constexpr const char UsbDeviceTask::NAME[];
constexpr const char UsbHostTask::NAME[];
constexpr const char SwitcherTask::NAME[];
constexpr const char LogicTask::NAME[];
constexpr const char OutputManagerTask::NAME[];
constexpr const char DatabaseTask::NAME[];
constexpr const char DatabaseTask::DB_NAME[];
constexpr const char WatchdogTask::NAME[];

}  // namespace config

}  // namespace tasks
