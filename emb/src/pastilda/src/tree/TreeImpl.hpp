/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREE_IMPL_H_
#define TREE_IMPL_H_

#include <config/TasksConfig.h>
#include <database/Entry.h>
#include <utils/patterns/Singleton.hpp>

#include "Tree.hpp"

namespace tree {

class TreeImpl final
  : public utils::patterns::Singleton<TreeImpl>
  , public tree::Tree<tasks::config::DatabaseTask::NODE_COUNT, db::Entry>
{
    friend class utils::patterns::Singleton<TreeImpl>;

 private:
    TreeImpl() : Tree(){};
    ~TreeImpl() = delete;
    TreeImpl(const TreeImpl& root);
    TreeImpl& operator=(const TreeImpl&);
};

};  // namespace tree

#endif /* TREE_IMPL_H_ */
