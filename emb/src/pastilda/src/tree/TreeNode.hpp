/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREE_TREENODE_HPP_
#define TREE_TREENODE_HPP_

#include <functional>
#include <string>

#include <etl/vector.h>

namespace tree {

template<typename T>
class TreeNode
{
 public:
    using TreeNodePtr = TreeNode*;
    using Container = T;

    TreeNode();
    ~TreeNode();

    void set_container(const Container& container) { _container = container; }

    Container& get_container() { return _container; }

    const Container& get_container() const { return _container; }

    void set_parent(TreeNodePtr parent) { _parent = parent; }

    TreeNodePtr get_parent() { return _parent; }

    void add_child(TreeNodePtr child);
    TreeNodePtr get_child_at(const std::size_t i) const;

    std::size_t get_children_count() { return _children_count; }

    bool is_end_node() const { return (_children == nullptr); }

    void add_left_neighbor(TreeNodePtr neighbor);
    void add_right_neighbor(TreeNodePtr neighbor);

    TreeNodePtr get_left_neighbor() const { return _left_neighbor; }

    TreeNodePtr get_right_neighbor() const { return _right_neighbor; }

    friend void destroy_tree_node(TreeNode* node)
    {
        node->set_parent(nullptr);
        node->_set_right_neighbor(nullptr);
        node->_set_left_neighbor(nullptr);
        node->_children = nullptr;
        node->_children_count = 0;
    }

 protected:
    Container _container;

    TreeNodePtr _parent;
    std::size_t _children_count;
    TreeNodePtr _children;
    TreeNodePtr _left_neighbor;
    TreeNodePtr _right_neighbor;

    void _add_first_child(TreeNodePtr child);
    void _set_left_neighbor(TreeNodePtr neighbor);
    void _set_right_neighbor(TreeNodePtr neighbor);
};

template<typename T>
TreeNode<T>::TreeNode() :
  _parent(nullptr),
  _children_count(0),
  _children(nullptr),
  _left_neighbor(nullptr),
  _right_neighbor(nullptr)
{
}

template<typename T>
TreeNode<T>::~TreeNode()
{
    destroy_tree_node(this);
}

template<typename T>
inline void TreeNode<T>::_add_first_child(TreeNodePtr child)
{
    _children = child;
}

template<typename T>
void TreeNode<T>::add_child(TreeNodePtr child)
{
    _children_count++;

    child->set_parent(this);

    if (_children_count == 1) {
        _add_first_child(child);
    }
    else {
        _children->add_right_neighbor(child);
    }
}

template<typename T>
void TreeNode<T>::add_left_neighbor(TreeNodePtr neighbor)
{
    if (_left_neighbor == nullptr) {
        _left_neighbor = neighbor;
        neighbor->_set_right_neighbor(this);
    }
    else {
        _left_neighbor->add_left_neighbor(neighbor);
    }
}

template<typename T>
void TreeNode<T>::add_right_neighbor(TreeNodePtr neighbor)
{
    if (_right_neighbor == nullptr) {
        _right_neighbor = neighbor;
        neighbor->_set_left_neighbor(this);
    }
    else {
        _right_neighbor->add_right_neighbor(neighbor);
    }
}

template<typename T>
inline void TreeNode<T>::_set_left_neighbor(TreeNodePtr neighbor)
{
    _left_neighbor = neighbor;
}

template<typename T>
inline void TreeNode<T>::_set_right_neighbor(TreeNodePtr neighbor)
{
    _right_neighbor = neighbor;
}

template<typename T>
typename TreeNode<T>::TreeNodePtr TreeNode<T>::get_child_at(
  const std::size_t childNum) const
{
    if (this->is_end_node()) {
        return nullptr;
    }

    std::size_t i = 0;
    TreeNodePtr child = _children;
    while (i < childNum) {
        child = child->get_right_neighbor();
        ++i;
    }

    return child;
}

} /* namespace tree */

#endif /* TREE_TREENODE_HPP_ */
