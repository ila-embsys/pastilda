#include <app/App.hpp>

#include <tasks/UsbDeviceTask.h>
#include <tasks/UsbHostTask.h>

namespace app {

App::App() :
  usb_device_task(tasks::UsbDeviceTask::Instance()),
  usb_host_task(tasks::UsbHostTask::Instance())
{
}

int App::exec() const
{
    rtos::Thread::StartScheduler();

    while (true) {
        __asm("nop");
    }

    return 0;
}

}  // namespace app
