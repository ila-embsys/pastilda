/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstring>
#include <utility>

#include <etl/flat_map.h>
#include <utils/memory/Dict.hpp>

#include "AutotypeReader.h"

namespace utils {

namespace keepass {

struct Keys
  : public utils::memory::
      FlatDict<AutotypeReader::StringType, AutotypeReader::Result::Data::Key, 2>
{
    Keys()
    {
        _add(Key("TAB"), Value::TAB);
        _add(Key("ENTER"), Value::ENTER);
    }
} static keys;

struct Credentials
  : public utils::memory::
      FlatDict<AutotypeReader::StringType, AutotypeReader::Result::Token, 2>
{
    using Key = AutotypeReader::StringType;
    using Value = AutotypeReader::Result::Token;

    Credentials()
    {
        _add(Key("USERNAME"), Value::USERNAME);
        _add(Key("PASSWORD"), Value::PASSWORD);
    }
} static credentials;

AutotypeReader::StringType delay_str = "DELAY";

auto AutotypeReader::read() -> Result
{
    if (_reading_input_str_pos == _input_string.end()) {
        Result result;
        result.token = Result::Token::FINISH;

        return result;
    }

    _parse_input();

    if (_is_error) {
        _is_error = false;
        _parentheses_control = 0;
        _reading_input_str_pos = _input_string.end();

        Result result;
        result.token = Result::Token::NON_VALID;

        return result;
    }

    if (_is_special_str) {
        return _get_special();
    }

    Result result;
    result.token = Result::Token::STRING;
    result.data.str = _parsing_string;

    return result;
}

void AutotypeReader::_parse_input()
{
    uint32_t parsing_string_length = 0;
    auto start_pose = _reading_input_str_pos;
    bool is_parsing_ended = false;

    _is_special_str = false;
    _is_error = false;

    while (not is_parsing_ended &&
           _reading_input_str_pos != _input_string.end() && not _is_error) {
        char c = *_reading_input_str_pos;

        switch (c) {
            case '{':
                // Valid parentheses control
                if (_parentheses_control % 2 != 0) {
                    _is_error = true;
                    break;
                }
                _parentheses_control++;

                // If we have some string before braсe
                if (parsing_string_length > 0) {
                    _parsing_string =
                      StringType(&*start_pose, parsing_string_length);
                    parsing_string_length = 0;
                    is_parsing_ended = true;
                }

                // Ommit '{' symbol
                start_pose++;
                break;

            case '}':
                // Valid parentheses control
                if (_parentheses_control % 2 == 0) {
                    _is_error = true;
                    break;
                }
                _parentheses_control--;

                // Stop parsing
                _is_special_str = true;
                _parsing_string =
                  StringType(&*start_pose, parsing_string_length);
                parsing_string_length = 0;
                is_parsing_ended = true;
                break;

            default:
                parsing_string_length++;
                break;
        }

        _reading_input_str_pos++;
    }

    if (_is_error) {
        return;
    }

    if (not is_parsing_ended) {
        _parsing_string = StringType(&*start_pose, parsing_string_length);
    }
}

auto AutotypeReader::_get_special() -> Result
{
    auto key = keys.get(_parsing_string);
    if (key != keys.not_a_value()) {
        Result result;
        result.token = Result::Token::KEY;
        result.data.key = key->second;

        return result;
    }

    auto credential = credentials.get(_parsing_string);
    if (credential != credentials.not_a_value()) {
        Result result;
        result.token = credential->second;

        return result;
    }

    if (_parsing_string.find(delay_str) != StringType::npos) {
        Result result;
        result.token = Result::Token::DELAY;
        result.data.delay = _get_delay();

        return result;
    }

    Result result;
    result.token = Result::Token::NON_VALID;

    return result;
}

uint32_t AutotypeReader::_get_delay()
{
    uint16_t num_length = _parsing_string.length() - delay_str.length() - 1;
    if (num_length == 0 || num_length > MAX_DELAY_DIGIT_COUNT)
        return 0;

    char s_num[MAX_DELAY_DIGIT_COUNT + 1] = {0};
    const char* s = _parsing_string.data() + delay_str.length();

    if ((*s != ' ') && (*s != '=')) {
        return 0;
    }

    std::memcpy(s_num, s + 1, num_length);

    return std::atoi(s_num);
}

}  // namespace keepass

}  // namespace utils
