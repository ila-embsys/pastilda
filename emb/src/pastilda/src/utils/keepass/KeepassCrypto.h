/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_KEEPASSCRYPTO_H_
#define UTILS_KEEPASSCRYPTO_H_

#include <stdint.h>
#include <string.h>

#include <crypto/base64.h>
#include <crypto/salsa20.h>
#include <libopencm3/stm32/crypto.h>
BEGIN_DECLS
#include <crypto/sha2.h>
END_DECLS

namespace utils {

namespace keepass {

class KeepassCrypto
{
 public:
#pragma pack(push, 1)
    struct ShaData
    {
        cf_sha256_context ctx;
        uint8_t buffer[320];
        uint32_t data_in;
    };
#pragma pack(pop)

    KeepassCrypto() = default;
    ~KeepassCrypto() = default;

    // SHA256 API////////////////////////////////////////////////
    //----------------------------------------------------------
    // Full SHA256 hashing. Applicable for a small messages (< 256B).
    // If you have larger message you should use the next 3 functions.
    static void SHA256_eval(const uint8_t* data, uint32_t len, uint8_t* hash);
    // Sets up context of SHA256 ready to hash a new message.
    static void SHA256_init(ShaData* sha);
    // Hashes next chunk of a huge message (> 256B).
    static void SHA256_proc(ShaData* sha);
    // Destroys context of SHA256 and finishes the hash operation.
    static void SHA256_stop(ShaData* sha, uint8_t* hash);

    // AES256 API////////////////////////////////////////////////
    //----------------------------------------------------------
    static void AES_EBC_encrypt(uint8_t* key,
                                uint8_t* data,
                                uint32_t data_len,
                                uint32_t cycles);
    static void AES_CBC_decrypt(uint8_t* key,
                                uint8_t* iv,
                                uint8_t* data,
                                uint32_t data_len);
    static void AES_CBC_init(uint8_t* key, uint8_t* iv);
    static void AES_CBC_proc(uint8_t* data, uint32_t data_len);
    static void AES_CBC_stop();

    // SALSA20 API///////////////////////////////////////////////
    //----------------------------------------------------------
    static void init_Salsa20(uint8_t* key, const uint8_t* iv);
    static void eval_Salsa20(uint8_t* input, uint32_t length);

 private:
    static constexpr uint32_t SHA256_BUFFER_SIZE = 320;
    static constexpr uint32_t SHA256_BLOCK_SIZE = 64;
};

} /* namespace keepass */

} /* namespace utils */

#endif /* UTILS_KEEPASSCRYPTO_H_ */
