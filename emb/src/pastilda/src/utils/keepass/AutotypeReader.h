/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_KEEPASS_AUTOTYPE_READER_HPP_
#define UTILS_KEEPASS_AUTOTYPE_READER_HPP_

#include <strs/StringView.hpp>

namespace utils {

namespace keepass {

class AutotypeReader final
{
 public:
    explicit AutotypeReader()
    {
        _reading_input_str_pos = _input_string.begin();
    }
    ~AutotypeReader() = default;

    static constexpr uint16_t MAX_DELAY_DIGIT_COUNT = 32;

    using StringType = strs::StringView;

    struct Result
    {
        enum Token
        {
            KEY,
            STRING,
            DELAY,
            FINISH,
            USERNAME,
            PASSWORD,
            NON_VALID
        };

        struct Data
        {
         public:
            using String = StringType;

            enum Key
            {
                TAB,
                ENTER,
            };

            Key key;
            String str;
            uint32_t delay;

            Data() { new (&str) String(); }
            ~Data() { str.~String(); }
        };

        Token token;
        Data data;
    };

    void set_input(StringType str)
    {
        _input_string = str;
        _reading_input_str_pos = _input_string.begin();
        _is_error = false;
        _parentheses_control = 0;
    }

    Result read();

 private:
    StringType _parsing_string;
    StringType _input_string;
    StringType::const_iterator _reading_input_str_pos;

    bool _is_special_str = false;
    bool _is_error = false;
    uint32_t _parentheses_control = 0;

    void _parse_input();
    uint32_t _get_delay();
    Result _get_special();
};

}  // namespace keepass

}  // namespace utils

#endif /* UTILS_KEEPASS_AUTOTYPE_READER_HPP_ */
