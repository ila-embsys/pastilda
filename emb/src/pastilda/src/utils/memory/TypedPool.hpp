/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_MEMORY_TYPEDPOOL_HPP_
#define UTILS_MEMORY_TYPEDPOOL_HPP_

#include <etl/pool.h>
#include <type_traits>

namespace utils {

namespace memory {

template<typename T, std::size_t N>
class TypedPool : public etl::ipool
{
 private:
    // The pool element.
    union Element
    {
        uintptr_t next;         ///< Pointer to the next free element.
        char value[sizeof(T)];  ///< Storage for value type.
        typename etl::type_with_alignment<etl::alignment_of<T>::value>::type
          dummy;  ///< Dummy item to get correct alignment.
    };

 public:
    using value_type = T;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;

    static constexpr auto SIZE = N;

    friend class iterator;

    class iterator
    {
     public:
        iterator() :
          _current_pool(nullptr),
          _current_element(nullptr),
          _index(-1)
        {
        }

        iterator(TypedPool* pool, Element* elem, uint32_t index) :
          _current_pool(pool),
          _current_element(elem),
          _index(index)
        {
        }

        iterator(const iterator& src)
        {
            _current_element = src._current_element;
            _index = src._index;
            _current_pool = src._current_pool;
        }

        iterator& operator=(const iterator& src)
        {
            _current_element = src._current_element;
            _index = src._index;
            return *this;
        }

        const iterator& operator=(const iterator& src) const
        {
            _current_element = &src._current_element;
            _index = src._index;
            return *this;
        }

        iterator& operator++()
        {
            if (_index + 1 != _current_pool->size()) {
                _index++;
                _current_element++;
            }
            else {
                _index = -1;
                _current_element =
                  _current_pool->_get_element(_current_pool->size());
            }

            return *this;
        }

        iterator operator++(int)
        {
            iterator temp(*this);
            ++(*this);
            return temp;
        }

        iterator& operator--()
        {
            if (_index != 0) {
                --_index;
                --_current_element;
            }

            return *this;
        }

        iterator operator--(int)
        {
            iterator temp(*this);
            --(*this);
            return temp;
        }

        reference operator*()
        {
            return *reinterpret_cast<pointer>(&_current_element->value);
        }

        pointer operator->()
        {
            return reinterpret_cast<pointer>(&_current_element->value);
        }

        //*******************************
        friend bool operator==(const iterator& lhs, const iterator& rhs)
        {
            return (lhs._current_element == rhs._current_element) &&
                   (lhs._index == rhs._index);
        }

        //*******************************
        friend bool operator!=(const iterator& lhs, const iterator& rhs)
        {
            return !(lhs == rhs);
        }

     private:
        TypedPool* _current_pool;
        Element* _current_element;
        uint32_t _index;
    };

    using const_iterator = const iterator;

    struct MemoryChunk
    {
        iterator start;
        iterator end;
    };

    TypedPool() :
      etl::ipool(reinterpret_cast<char*>(&_buffer[0]), _ELEMENT_SIZE, SIZE)
    {
    }

    virtual ~TypedPool() {}

    value_type* allocate_and_construct()
    {
        if (not std::is_fundamental<value_type>::value) {
            return new (this->template allocate<value_type>()) value_type();
        }
        return this->template allocate<value_type>();
    }

    MemoryChunk allocate_n(std::size_t n)
    {
        MemoryChunk chunk;
        this->template allocate<value_type>();
        chunk.start = iterator(this, _get_element(size() - 1), size() - 1);
        for (uint32_t i = 0; i < n; ++i) {
            this->template allocate<value_type>();
        }
        chunk.end = iterator(this, _get_element(size() - 1), size() - 1);
        return chunk;
    }

    iterator begin() { return iterator(this, _get_element(0), 0); }

    iterator end() { return iterator(this, _get_element(size()), -1); }

 private:
    ///< The memory for the pool of objects.
    typename etl::aligned_storage<sizeof(Element),
                                  etl::alignment_of<Element>::value>::type
      _buffer[SIZE];

    static constexpr uint32_t _ELEMENT_SIZE = sizeof(Element);

    TypedPool(const TypedPool&);
    TypedPool& operator=(const TypedPool&);

    Element* _get_element(std::size_t i)
    {
        return reinterpret_cast<Element*>(&_buffer[i]);
    }
};

}  // namespace memory

}  // namespace utils

#endif /* UTILS_MEMORY_TYPEDPOOL_HPP_ */
