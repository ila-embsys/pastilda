/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "KeyObj.h"
#include <lang/Ascii.h>

namespace utils {

namespace kbd {

using namespace lang;

static Ascii::Code kbd_key_to_ascii_code[Ascii::EXTENDED_CODES_COUNT] = {
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::UPPERCASE_A,
  Ascii::UPPERCASE_B,
  Ascii::UPPERCASE_C,
  Ascii::UPPERCASE_D,
  Ascii::UPPERCASE_E,
  Ascii::UPPERCASE_F,
  Ascii::UPPERCASE_G,
  Ascii::UPPERCASE_H,
  Ascii::UPPERCASE_I,
  Ascii::UPPERCASE_J,
  Ascii::UPPERCASE_K,
  Ascii::UPPERCASE_L,
  Ascii::UPPERCASE_M,
  Ascii::UPPERCASE_N,
  Ascii::UPPERCASE_O,
  Ascii::UPPERCASE_P,
  Ascii::UPPERCASE_Q,
  Ascii::UPPERCASE_R,
  Ascii::UPPERCASE_S,
  Ascii::UPPERCASE_T,
  Ascii::UPPERCASE_U,
  Ascii::UPPERCASE_V,
  Ascii::UPPERCASE_W,
  Ascii::UPPERCASE_X,
  Ascii::UPPERCASE_Y,
  Ascii::UPPERCASE_Z,
  Ascii::EXCLAMATION_MARK,
  Ascii::AT_SYMBOL,
  Ascii::NUMBER,
  Ascii::DOLLAR,
  Ascii::PERCENT,
  Ascii::CARET,
  Ascii::AMPERSAND,
  Ascii::ASTERISK,
  Ascii::OPEN_PARENTHESIS,
  Ascii::CLOSE_PARENTHESIS,
  Ascii::LF,
  Ascii::ESC,
  Ascii::BACKSPACE,
  Ascii::TAB,
  Ascii::SPACE,
  Ascii::UNDERSCORE,
  Ascii::EQUALS,
  Ascii::OPENING_BRACKET,
  Ascii::CLOSING_BRACKET,
  Ascii::BACKSLASH,
  Ascii::NUL,  // KEY_NONUS_NUMBER_SIGN_TILDE
  Ascii::SEMICOLON,
  Ascii::SINGLE_QUOTE,
  Ascii::GRAVE_ACCENT,
  Ascii::COMMA,
  Ascii::PERIOD_OR_DOT,
  Ascii::SLASH_OR_DIVIDE,
  Ascii::NUL,  // Caps Lock
  Ascii::NUL,  // F1
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,  // F12
  Ascii::NUL,  // KEY_PRINTSCREEN
  Ascii::NUL,  // KEY_SCROLL_LOCK
  Ascii::NUL,  // KEY_PAUSE
  Ascii::NUL,  // KEY_INSERT
  Ascii::NUL,  // KEY_HOME
  Ascii::NUL,  // KEY_PAGEUP
  Ascii::DELETE,
  Ascii::NUL,    // KEY_END1
  Ascii::NUL,    // KEY_PAGEDOWN
  Ascii::RIGHT,  // KEY_RIGHTARROW
  Ascii::LEFT,   // KEY_LEFTARROW
  Ascii::DOWN,   // KEY_DOWNARROW
  Ascii::UP,     // KEY_UPARROW
  Ascii::NUL,    // KEY_KEYPAD_NUM_LOCK_AND_CLEAR
  Ascii::SLASH_OR_DIVIDE,
  Ascii::ASTERISK,
  Ascii::HYPHEN,
  Ascii::PLUS,
  Ascii::LF,
  Ascii::ONE,
  Ascii::TWO,
  Ascii::THREE,
  Ascii::FOUR,
  Ascii::FIVE,
  Ascii::SIX,
  Ascii::SEVEN,
  Ascii::EIGHT,
  Ascii::NINE,
  Ascii::ZERO,
  Ascii::PERIOD_OR_DOT,
  Ascii::SLASH_OR_DIVIDE,
  Ascii::NUL,  // KEY_APPLICATION
  Ascii::ASTERISK,
  Ascii::EQUALS,
  Ascii::NUL,  // F13
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,
  Ascii::NUL,  // F24
  Ascii::NUL,  // KEY_EXECUTE
  Ascii::NUL,  // KEY_HELP
  Ascii::NUL,  // KEY_MENU
  Ascii::NUL,  // KEY_SELECT
  Ascii::NUL,  // KEY_STOP
  Ascii::NUL,  // KEY_AGAIN
  Ascii::NUL,  // KEY_UNDO
  Ascii::NUL,  // KEY_CUT
  Ascii::NUL,  // KEY_COPY
  Ascii::NUL,  // KEY_PASTE
  Ascii::NUL   // KEY_FIND
};

static usbkbd::Key ascii_code_to_usb_key[Ascii::EXTENDED_CODES_COUNT] = {
  usbkbd::Key::NOT_A_KEY,                    // 0x00 Null
  usbkbd::Key::NOT_A_KEY,                    // 0x01
  usbkbd::Key::NOT_A_KEY,                    // 0x02
  usbkbd::Key::NOT_A_KEY,                    // 0x03
  usbkbd::Key::NOT_A_KEY,                    // 0x04
  usbkbd::Key::NOT_A_KEY,                    // 0x05
  usbkbd::Key::NOT_A_KEY,                    // 0x06
  usbkbd::Key::NOT_A_KEY,                    // 0x07
  usbkbd::Key::KEY_BACKSPACE,                // 0x08 Backspace
  usbkbd::Key::KEY_TAB,                      // 0x09 Horizontal Tab
  usbkbd::Key::KEY_ENTER,                    // 0x0A Line Feed
  usbkbd::Key::NOT_A_KEY,                    // 0x0B
  usbkbd::Key::NOT_A_KEY,                    // 0x0C
  usbkbd::Key::KEY_ENTER,                    // 0x0D Carriage return
  usbkbd::Key::NOT_A_KEY,                    // 0x0E
  usbkbd::Key::NOT_A_KEY,                    // 0x0F
  usbkbd::Key::NOT_A_KEY,                    // 0x10
  usbkbd::Key::NOT_A_KEY,                    // 0x11
  usbkbd::Key::NOT_A_KEY,                    // 0x12
  usbkbd::Key::NOT_A_KEY,                    // 0x13
  usbkbd::Key::NOT_A_KEY,                    // 0x14
  usbkbd::Key::NOT_A_KEY,                    // 0x15
  usbkbd::Key::NOT_A_KEY,                    // 0x16
  usbkbd::Key::NOT_A_KEY,                    // 0x17
  usbkbd::Key::NOT_A_KEY,                    // 0x18
  usbkbd::Key::NOT_A_KEY,                    // 0x19
  usbkbd::Key::NOT_A_KEY,                    // 0x1A
  usbkbd::Key::KEY_ESCAPE,                   // 0x1B Escape
  usbkbd::Key::NOT_A_KEY,                    // 0x1C
  usbkbd::Key::NOT_A_KEY,                    // 0x1D
  usbkbd::Key::NOT_A_KEY,                    // 0x1E
  usbkbd::Key::NOT_A_KEY,                    // 0x1F
  usbkbd::Key::KEY_SPACEBAR,                 // 0x20
  usbkbd::Key::KEY_1_EXCLAMATION_MARK,       // 0x21 !
  usbkbd::Key::KEY_2_AT,                     // 0x22 "
  usbkbd::Key::KEY_3_NUMBER_SIGN,            // 0x23 #
  usbkbd::Key::KEY_4_DOLLAR,                 // 0x24 $
  usbkbd::Key::KEY_5_PERCENT,                // 0x25 %
  usbkbd::Key::KEY_7_AMPERSAND,              // 0x26 &
  usbkbd::Key::KEY_SINGLE_AND_DOUBLE_QUOTE,  // 0x27 '
  usbkbd::Key::KEY_9_OPARENTHESIS,           // 0x28 (
  usbkbd::Key::KEY_0_CPARENTHESIS,           // 0x29 )
  usbkbd::Key::KEY_8_ASTERISK,               // 0x2A *
  usbkbd::Key::KEY_EQUAL_PLUS,               // 0x2B +
  usbkbd::Key::KEY_COMMA_AND_LESS,           // 0x2C ,
  usbkbd::Key::KEY_MINUS_UNDERSCORE,         // 0x2D -
  usbkbd::Key::KEY_DOT_GREATER,              // 0x2E .
  usbkbd::Key::KEY_SLASH_QUESTION,           // 0x2F /
  usbkbd::Key::KEY_0_CPARENTHESIS,           // 0x30 0
  usbkbd::Key::KEY_1_EXCLAMATION_MARK,       // 0x31 1
  usbkbd::Key::KEY_2_AT,                     // 0x32 2
  usbkbd::Key::KEY_3_NUMBER_SIGN,            // 0x33 3
  usbkbd::Key::KEY_4_DOLLAR,                 // 0x34 4
  usbkbd::Key::KEY_5_PERCENT,                // 0x35 5
  usbkbd::Key::KEY_6_CARET,                  // 0x36 6
  usbkbd::Key::KEY_7_AMPERSAND,              // 0x37 7
  usbkbd::Key::KEY_8_ASTERISK,               // 0x38 8
  usbkbd::Key::KEY_9_OPARENTHESIS,           // 0x39 9
  usbkbd::Key::KEY_SEMICOLON_COLON,          // 0x3A :
  usbkbd::Key::KEY_SEMICOLON_COLON,          // 0x3B ;
  usbkbd::Key::KEY_COMMA_AND_LESS,           // 0x3C <
  usbkbd::Key::KEY_EQUAL_PLUS,               // 0x3D =
  usbkbd::Key::KEY_DOT_GREATER,              // 0x3E >
  usbkbd::Key::KEY_SLASH_QUESTION,           // 0x3F ?
  usbkbd::Key::KEY_2_AT,                     // 0x40 @
  usbkbd::Key::KEY_A,                        // 0x41 A
  usbkbd::Key::KEY_B,                        // 0x42 B
  usbkbd::Key::KEY_C,                        // 0x43 C
  usbkbd::Key::KEY_D,                        // 0x44 D
  usbkbd::Key::KEY_E,                        // 0x45 E
  usbkbd::Key::KEY_F,                        // 0x46 F
  usbkbd::Key::KEY_G,                        // 0x47 G
  usbkbd::Key::KEY_H,                        // 0x48 H
  usbkbd::Key::KEY_I,                        // 0x49 I
  usbkbd::Key::KEY_J,                        // 0x4A J
  usbkbd::Key::KEY_K,                        // 0x4B K
  usbkbd::Key::KEY_L,                        // 0x4C L
  usbkbd::Key::KEY_M,                        // 0x4D M
  usbkbd::Key::KEY_N,                        // 0x4E N
  usbkbd::Key::KEY_O,                        // 0x4F O
  usbkbd::Key::KEY_P,                        // 0x50 P
  usbkbd::Key::KEY_Q,                        // 0x51 Q
  usbkbd::Key::KEY_R,                        // 0x52 R
  usbkbd::Key::KEY_S,                        // 0x53 S
  usbkbd::Key::KEY_T,                        // 0x55 T
  usbkbd::Key::KEY_U,                        // 0x55 U
  usbkbd::Key::KEY_V,                        // 0x56 V
  usbkbd::Key::KEY_W,                        // 0x57 W
  usbkbd::Key::KEY_X,                        // 0x58 X
  usbkbd::Key::KEY_Y,                        // 0x59 Y
  usbkbd::Key::KEY_Z,                        // 0x5A Z
  usbkbd::Key::KEY_OBRACKET_AND_OBRACE,      // 0x5B [
  usbkbd::Key::KEY_BACKSLASH_VERTICAL_BAR,   // 0x5C '\'
  usbkbd::Key::KEY_CBRACKET_AND_CBRACE,      // 0x5D ]
  usbkbd::Key::KEY_6_CARET,                  // 0x5E ^
  usbkbd::Key::KEY_MINUS_UNDERSCORE,         // 0x5F _
  usbkbd::Key::KEY_GRAVE_ACCENT_AND_TILDE,   // 0x60 `
  usbkbd::Key::KEY_A,                        // 0x61 a
  usbkbd::Key::KEY_B,                        // 0x62 b
  usbkbd::Key::KEY_C,                        // 0x63 c
  usbkbd::Key::KEY_D,                        // 0x66 d
  usbkbd::Key::KEY_E,                        // 0x65 e
  usbkbd::Key::KEY_F,                        // 0x66 f
  usbkbd::Key::KEY_G,                        // 0x67 g
  usbkbd::Key::KEY_H,                        // 0x68 h
  usbkbd::Key::KEY_I,                        // 0x69 i
  usbkbd::Key::KEY_J,                        // 0x6A j
  usbkbd::Key::KEY_K,                        // 0x6B k
  usbkbd::Key::KEY_L,                        // 0x6C l
  usbkbd::Key::KEY_M,                        // 0x6D m
  usbkbd::Key::KEY_N,                        // 0x6E n
  usbkbd::Key::KEY_O,                        // 0x6F o
  usbkbd::Key::KEY_P,                        // 0x70 p
  usbkbd::Key::KEY_Q,                        // 0x71 q
  usbkbd::Key::KEY_R,                        // 0x72 r
  usbkbd::Key::KEY_S,                        // 0x73 s
  usbkbd::Key::KEY_T,                        // 0x75 t
  usbkbd::Key::KEY_U,                        // 0x75 u
  usbkbd::Key::KEY_V,                        // 0x76 v
  usbkbd::Key::KEY_W,                        // 0x77 w
  usbkbd::Key::KEY_X,                        // 0x78 x
  usbkbd::Key::KEY_Y,                        // 0x79 y
  usbkbd::Key::KEY_Z,                        // 0x7A z
  usbkbd::Key::KEY_OBRACKET_AND_OBRACE,      // 0x7B {
  usbkbd::Key::KEY_BACKSLASH_VERTICAL_BAR,   // 0x7C |
  usbkbd::Key::KEY_CBRACKET_AND_CBRACE,      // 0x7D }
  usbkbd::Key::KEY_GRAVE_ACCENT_AND_TILDE,   // 0x7E ~
  usbkbd::Key::KEY_DELETE,                   // 0x7F Delete

  /* PSEUDO ASCII */
  usbkbd::Key::KEY_LEFTARROW,   // 0x80 LEFT key
  usbkbd::Key::KEY_RIGHTARROW,  // 0x81 RIGHT key
  usbkbd::Key::KEY_UPARROW,     // 0x82 UP key
  usbkbd::Key::KEY_DOWNARROW    // 0x83 DOWN key
};

Ascii::Code get_ascii_by_kbd_key(usbkbd::Key code)
{
    return kbd_key_to_ascii_code[static_cast<Ascii::Type>(code)];
}

usbkbd::Key get_usb_kbd_by_ascii(Ascii::Code code)
{
    return ascii_code_to_usb_key[static_cast<Ascii::Type>(code)];
}

KeyObj::KeyObj() :
  _kbd_key(usbkbd::Key::NOT_A_KEY),
  _ascii(Ascii::NUL),
  _key_type(KeyType::SYBMOL)
{
}

KeyObj::KeyObj(usbkbd::KeyCodeType key, usbkbd::KeyCodeType modifier)
{
    set(key, modifier);
}

KeyObj::KeyObj(usbkbd::Key key, usbkbd::SpecialKeySequence modifier)
{
    set(key, modifier);
}

KeyObj::KeyObj(Ascii code)
{
    set(code);
}

KeyObj::KeyObj(usbkbd::KeyCodeType key, usbkbd::SpecialKey modifier)
{
    set(key, static_cast<usbkbd::KeyCodeType>(modifier));
}

KeyObj::KeyObj(usbkbd::Key key, usbkbd::KeyCodeType modifier)
{
    set(static_cast<usbkbd::KeyCodeType>(key), modifier);
}

KeyObj::KeyObj(usbkbd::Key key, usbkbd::SpecialKey modifier)
{
    set(key, modifier);
}

KeyObj::KeyObj(usbkbd::Key key)
{
    set(key, usbkbd::SpecialKey::NO_KEY);
}

KeyObj::~KeyObj() {}

void KeyObj::set(Ascii code)
{
    _ascii = code;
    _kbd_key = get_usb_kbd_by_ascii(_ascii);
    _kbd_key_modifier.clear();

    if (_ascii.is_shifted()) {
        _kbd_key_modifier = usbkbd::SpecialKey::LEFT_SHIFT;
        _kbd_key = get_usb_kbd_by_ascii(get_ascii_shifted_back(_ascii));
    }

    _update_key_type();
}

void KeyObj::set(usbkbd::Key key, usbkbd::SpecialKey modifier)
{
    set(static_cast<usbkbd::KeyCodeType>(key),
        static_cast<usbkbd::KeyCodeType>(modifier));
}

void KeyObj::set(usbkbd::KeyCodeType key, usbkbd::KeyCodeType modifier)
{
    _kbd_key = static_cast<usbkbd::Key>(key);
    _kbd_key_modifier = modifier;

    _ascii = get_ascii_by_kbd_key(_kbd_key);

    _check_ascii_shifted();

    _update_key_type();
}

void KeyObj::set(usbkbd::Key key, usbkbd::SpecialKeySequence modifier)
{
    _kbd_key = key;
    _kbd_key_modifier = modifier;

    _ascii = get_ascii_by_kbd_key(_kbd_key);

    _check_ascii_shifted();

    _update_key_type();
}

void KeyObj::_check_ascii_shifted()
{
    if (_ascii.code() != Ascii::NUL) {
        bool usbModifierIsShift = (_kbd_key_modifier == usbkbd::SpecialKey::LEFT_SHIFT ||
                                   _kbd_key_modifier == usbkbd::SpecialKey::RIGHT_SHIFT);

        if (usbModifierIsShift) {
            if (!_ascii.is_shifted()) {
                _ascii = _ascii.get_shifted();
            }
        }
        else {
            if (_ascii.is_shifted()) {
                _ascii = _ascii.get_shifted_back();
            }
        }
    }
}

void KeyObj::_update_key_type()
{
    if (_kbd_key == usbkbd::Key::KEY_RIGHTARROW ||
        _kbd_key == usbkbd::Key::KEY_LEFTARROW ||
        _kbd_key == usbkbd::Key::KEY_UPARROW ||
        _kbd_key == usbkbd::Key::KEY_DOWNARROW ||
        _kbd_key == usbkbd::Key::KEY_ENTER ||
        _kbd_key == usbkbd::Key::KEY_ESCAPE) {
        _key_type = CONTROL;
    }
    else {
        _key_type = SYBMOL;
    }
}

}  // namespace kbd

} /* namespace utils */
