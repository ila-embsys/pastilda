/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_KEYS_KEY_H_
#define UTILS_KEYS_KEY_H_

#include <cstdint>

#include <hw/kbd/KeyCodes.hpp>
#include <lang/Ascii.h>

namespace utils {

namespace kbd {

namespace usbkbd = hw::kbd;

lang::Ascii::Code get_ascii_by_kbd_key(usbkbd::Key code);
usbkbd::Key get_usb_kbd_by_ascii(lang::Ascii::Code code);

class KeyObj
{
 public:
    enum KeyType
    {
        CONTROL,
        SYBMOL
    };

    KeyObj();
    KeyObj(usbkbd::Key key);
    KeyObj(usbkbd::KeyCodeType key, usbkbd::KeyCodeType modifier);
    KeyObj(usbkbd::KeyCodeType key, usbkbd::SpecialKey modifier);
    KeyObj(usbkbd::Key key, usbkbd::KeyCodeType modifier);
    KeyObj(usbkbd::Key key, usbkbd::SpecialKeySequence modifier);
    KeyObj(usbkbd::Key key, usbkbd::SpecialKey modifier);
    KeyObj(lang::Ascii code);
    ~KeyObj();

    void set(usbkbd::KeyCodeType key, usbkbd::KeyCodeType modifier);
    void set(usbkbd::Key key, usbkbd::SpecialKeySequence modifier);
    void set(usbkbd::Key key, usbkbd::SpecialKey modifier);
    void set(lang::Ascii code);

    usbkbd::KeyCodeType get_usb_key_code() const
    {
        return static_cast<usbkbd::KeyCodeType>(_kbd_key);
    }

    usbkbd::Key get_kbd_key() const { return _kbd_key; }

    usbkbd::SpecialKeySequence get_kbd_key_modifier() const
    {
        return _kbd_key_modifier;
    }

    usbkbd::KeyCodeType get_kbd_key_modifier_mask()
    {
        return _kbd_key_modifier.get_mask();
    }

    lang::Ascii::Code get_ascii_code() const
    {
        return static_cast<lang::Ascii::Code>(_ascii);
    }

    lang::Ascii get_ascii() const { return _ascii; }

    bool is_control() const { return (_key_type == CONTROL); }

    bool is_shifted() const { return _ascii.is_shifted(); }

    bool operator==(const KeyObj& rhs) const
    {
        return (rhs._kbd_key == _kbd_key) &&
               (rhs._kbd_key_modifier == _kbd_key_modifier);
    }

    bool operator!=(const KeyObj& rhs) const
    {
        return !((rhs._kbd_key == _kbd_key) &&
                 (rhs._kbd_key_modifier == _kbd_key_modifier));
    }

    friend bool operator==(const KeyObj& lhs, const usbkbd::Key rhs)
    {
        return (lhs._kbd_key == rhs);
    }

    friend bool operator!=(KeyObj& lhs, usbkbd::Key rhs)
    {
        return !(lhs._kbd_key == rhs);
    }

    friend bool operator==(const usbkbd::Key rhs, const KeyObj& lhs)
    {
        return (lhs._kbd_key == rhs);
    }

    friend bool operator==(const KeyObj& lhs, const lang::Ascii& rhs)
    {
        return (lhs._ascii == rhs);
    }

    friend bool operator==(const lang::Ascii& lhs, const KeyObj& rhs)
    {
        return (lhs == rhs._ascii);
    }

    KeyObj& operator=(const KeyObj& rhs)
    {
        _ascii = rhs.get_ascii();
        _kbd_key = rhs.get_kbd_key();
        _kbd_key_modifier = rhs.get_kbd_key_modifier();
        _update_key_type();
        return *this;
    }

    KeyObj& operator=(const usbkbd::Key& rhs)
    {
        set(rhs, usbkbd::SpecialKey::NO_KEY);
        return *this;
    }

 private:
    usbkbd::SpecialKeySequence _kbd_key_modifier;
    usbkbd::Key _kbd_key;

    lang::Ascii _ascii;

    KeyType _key_type;

    void _check_ascii_shifted();
    void _update_key_type();
};

}  // namespace kbd

} /* namespace utils */

#endif /* UTILS_KEYS_KEY_H_ */
