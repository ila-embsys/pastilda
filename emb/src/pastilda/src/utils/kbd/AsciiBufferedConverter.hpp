/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PACKGES_PACKAGEFACTORY_H_
#define PACKGES_PACKAGEFACTORY_H_

#include <cstddef>

#include <hw/kbd/KeyCodes.hpp>
#include <hw/kbd/Package.hpp>
#include <lang/Ascii.h>
#include <strs/StringConst.hpp>
#include <strs/StringView.hpp>

#include "KeyObj.h"

using std::size_t;

namespace utils {

namespace kbd {

namespace usbkbd = hw::kbd;

template<typename T>
class AsciiBufferedConverter
{
 public:
    using ResultBuffer = T;

    enum class Status
    {
        FINISHED,
        AT_WORK
    };

 private:
    enum class State
    {
        FINISHED,
        AT_WORK
    };

 public:
    constexpr static size_t MAX_KEYS_COUNT_PER_PACKAGE =
      2;  // maximal count is 6

    AsciiBufferedConverter(ResultBuffer* buffer);
    ~AsciiBufferedConverter() = default;

    void parse(const lang::Ascii* input_data, size_t length);
    void parse(const char* input_data, size_t length);
    void parse(const strs::StringView& input_data);
    void parse(strs::StringConst& input_data);

    void set_input(strs::StringConst& input_data);
    void set_input(const strs::StringView& input_data);
    void set_input(const lang::Ascii* input_data, size_t length);
    void set_input(const char* input_data, size_t length);
    Status process_one(usbkbd::Package& pckg);

 private:
    constexpr static uint8_t _EMPTY_FIELD = usbkbd::PACKAGE_EMPTY_FIELD;

    State _state;

    ResultBuffer* _result_buffer;
    strs::StringView _input_string;
    strs::StringView::const_iterator _input_string_it;

    usbkbd::Package* _current_package;
    size_t _current_package_field_num;

    utils::kbd::KeyObj _last_key;

    void _process_input_string();

    void _add_empty_package();
    void _add_shifted_package(utils::kbd::KeyObj& key);
    void _add_simple_package(utils::kbd::KeyObj& key);
    void _continue_simple_package(utils::kbd::KeyObj& key);
    void _new_simple_package(utils::kbd::KeyObj& key);
    void _check_simple_package_end();
    void _complete_last_simple_package();
};

template<typename T>
AsciiBufferedConverter<T>::AsciiBufferedConverter(ResultBuffer* buffer) :
  _state(State::FINISHED),
  _result_buffer(buffer),
  _input_string_it(_input_string.begin()),
  _current_package(nullptr),
  _current_package_field_num(0),
  _last_key(usbkbd::Key::NOT_A_KEY)
{
}

template<typename T>
void AsciiBufferedConverter<T>::parse(const strs::StringView& input_string)
{
    set_input(input_string);
    _process_input_string();
}

template<typename T>
void AsciiBufferedConverter<T>::parse(const lang::Ascii* input_data,
                                      size_t length)
{
    set_input(input_data, length);
    _process_input_string();
}

template<typename T>
void AsciiBufferedConverter<T>::parse(const char* input_data, size_t length)
{
    set_input(input_data, length);
    _process_input_string();
}

template<typename T>
void AsciiBufferedConverter<T>::parse(strs::StringConst& input_data)
{
    set_input(input_data.data(), input_data.length());
    _process_input_string();
}

template<typename T>
void AsciiBufferedConverter<T>::set_input(const strs::StringView& input_string)
{
    _input_string = input_string;
    _input_string_it = _input_string.begin();
}

template<typename T>
void AsciiBufferedConverter<T>::set_input(const lang::Ascii* input_data,
                                          size_t length)
{
    _input_string =
      strs::StringView(reinterpret_cast<const char*>(input_data), length);
    _input_string_it = _input_string.begin();
}

template<typename T>
void AsciiBufferedConverter<T>::set_input(const char* input_data, size_t length)
{
    _input_string = strs::StringView(input_data, length);
    _input_string_it = _input_string.begin();
}

template<typename T>
void AsciiBufferedConverter<T>::set_input(strs::StringConst& input_data)
{
    set_input(input_data.data(), input_data.length());
    _input_string_it = _input_string.begin();
}

template<typename T>
auto AsciiBufferedConverter<T>::process_one(usbkbd::Package& pckg) -> Status
{
    if (_result_buffer->size() == 0) {
        if (_input_string_it == _input_string.end()) {
            _input_string_it = _input_string.begin();
            return Status::FINISHED;
        }

        _state = State::AT_WORK;

        while (_state != State::FINISHED) {
            auto c = *_input_string_it;

            utils::kbd::KeyObj key(c);

            if (key.is_shifted()) {
                _add_shifted_package(key);
            }
            else {
                _add_simple_package(key);
            }

            _input_string_it++;
        }

        _add_empty_package();
    }

    pckg = _result_buffer->back();
    _result_buffer->pop_back();

    return Status::AT_WORK;
}

template<typename T>
void AsciiBufferedConverter<T>::_process_input_string()
{
    for (auto c : _input_string) {
        utils::kbd::KeyObj key(c);

        if (key.is_shifted()) {
            _add_shifted_package(key);
        }
        else {
            _add_simple_package(key);
        }
    }

    _add_empty_package();
}

template<typename T>
void AsciiBufferedConverter<T>::_add_simple_package(utils::kbd::KeyObj& key)
{
    // If in one package will be two same keys one
    // by one, then the only one will be send. So we
    // should split package if we have this situation.
    if (_current_package_field_num == 0 || key == _last_key) {
        _new_simple_package(key);
    }
    else {
        _continue_simple_package(key);
    }

    _last_key = key;
    _check_simple_package_end();
}

template<typename T>
void AsciiBufferedConverter<T>::_add_shifted_package(utils::kbd::KeyObj& key)
{
    _add_empty_package();  // clear last key
    _add_empty_package();  // add new empty package

    usbkbd::Package& current_package = _result_buffer->back();
    current_package.special = key.get_kbd_key_modifier();
    current_package.keys[0] = key.get_kbd_key();

    _state = State::FINISHED;
}

template<typename T>
void AsciiBufferedConverter<T>::_add_empty_package()
{
    _complete_last_simple_package();

    _result_buffer->push_back(usbkbd::Package());
    _result_buffer->back().clear();
}

template<typename T>
void AsciiBufferedConverter<T>::_new_simple_package(utils::kbd::KeyObj& key)
{
    _add_empty_package();  // clear last key
    _add_empty_package();  // add new empty package

    usbkbd::Package& current_package = _result_buffer->back();
    current_package.special = key.get_kbd_key_modifier();
    current_package.reserved = _EMPTY_FIELD;
    current_package.keys[0] = key.get_kbd_key();

    _current_package_field_num = 2;  // last set field
}

template<typename T>
void AsciiBufferedConverter<T>::_continue_simple_package(
  utils::kbd::KeyObj& key)
{
    _current_package_field_num++;
    size_t currentKeyNum =
      _current_package_field_num - usbkbd::PACKAGE_SPECIAL_FIELDS_LENGTH;

    usbkbd::Package& current_package = _result_buffer->back();
    current_package.keys[currentKeyNum] = key.get_kbd_key();
}

template<typename T>
void AsciiBufferedConverter<T>::_check_simple_package_end()
{
    size_t max_package_field_num =
      usbkbd::PACKAGE_SPECIAL_FIELDS_LENGTH + MAX_KEYS_COUNT_PER_PACKAGE - 1;

    if (_current_package_field_num ==
        max_package_field_num) {          // 2 + 1 - 1 = 2,
        _complete_last_simple_package();  // what means, that there will be
    }                                     // only one key per package,
                                          // otherwise linux don't work
}

template<typename T>
void AsciiBufferedConverter<T>::_complete_last_simple_package()
{
    bool last_package_not_completed = (_current_package_field_num > 0);
    if (last_package_not_completed) {
        _current_package_field_num = 0;
    }

    _state = State::FINISHED;
}

} /* namespace kbd */

} /* namespace utils */

#endif /* PACKGES_PACKAGEFACTORY_H_ */
