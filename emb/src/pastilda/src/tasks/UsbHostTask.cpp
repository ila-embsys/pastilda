/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UsbHostTask.h"

namespace tasks {

UsbHostTask::UsbHostTask() :
  Thread(config::UsbHostTask::NAME,
         config::UsbHostTask::STACK_SIZE,
         config::UsbHostTask::PRIORITY),
  _usb(UsbHostTask::_usb_message_handler),
  _input_queue(10, 1)
{
    this->Start();
}

void UsbHostTask::Run()
{
    TickType_t delay_time = rtos::Ticks::MsToTicks(config::UsbHostTask::SLEEP_TIME);

    while (true) {
        Thread::Delay(delay_time);

        usbh_poll(_usb.get_time_us());

        if (is_hid_ready_for_set_report(0)) {
            if (_dequeue() == Action::SEND) {
                hid_set_report(0, _leds);
            }
        }
    }
}

void UsbHostTask::_usb_message_handler(uint8_t device_id,
                                       const uint8_t* data,
                                       uint32_t length)
{
    if (length == hw::kbd::PACKAGE_LENGTH) {
        msg::SwitcherData switcher_data;
        switcher_data.package = *(hw::kbd::Package*)data;

        SwitcherTask::MsgType message;
        message.token = SwitcherTask::MsgType::Token::KEYS_FROM_HOST;
        message.data = switcher_data;
        SwitcherTask::fifo_push(&message);
    }
}

void UsbHostTask::enqueue(uint8_t leds)
{
    UsbHostTask::Instance()._input_queue.Enqueue(&leds, 0);
}

auto UsbHostTask::_dequeue() -> Action
{
    if (_input_queue.Dequeue(&_leds, 0))
        return Action::SEND;

    return Action::NONE;
}

} /* namespace tasks */
