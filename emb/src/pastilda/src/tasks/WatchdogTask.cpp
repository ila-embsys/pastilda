/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin <d.lisin@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WatchdogTask.h"
#include <libopencm3/stm32/iwdg.h>

namespace tasks {

WatchdogTask::WatchdogTask() :
  Thread(config::WatchdogTask::NAME,
         config::WatchdogTask::STACK_SIZE,
         config::WatchdogTask::PRIORITY)
{
    iwdg_set_period_ms(config::WatchdogTask::WATCHDOG_PERIOD);
    this->Start();
}

void WatchdogTask::Run()
{
    iwdg_start();

    TickType_t reset_time =
      rtos::Ticks::MsToTicks(config::WatchdogTask::RESET_PERIOD);

    while (true) {
        // TODO: Check Pastilda system before watchdog reset
        iwdg_reset();
        Thread::Delay(reset_time);
    }
}

} /* namespace tasks */
