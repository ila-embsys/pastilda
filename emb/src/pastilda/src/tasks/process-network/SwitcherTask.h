/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_SWITCHERTASK_H_
#define TASKS_SWITCHERTASK_H_

#include <config/TasksConfig.h>
#include <etl/vector.h>
#include <hw/kbd/Package.hpp>
#include <tasks/UsbDeviceTask.h>
#include <tasks/UsbHostTask.h>
#include <utils/kbd/KbdBufferedConverter.h>

#include "LogicTask.h"
#include "common/IKahnProcessTask.hpp"
#include "common/Message.hpp"

namespace tasks {

namespace msg {

enum class SwitcherCommand : uint8_t
{
    KEYS_FROM_HOST,
    LEDS_FROM_DEVICE,

    KEYS_FROM_PASTILDA,
    LEDS_FROM_PASTILDA,

    KEYS_PAUSE_FROM_PASTILDA,
    LEDS_PAUSE_FROM_PASTILDA,

    PASTILD_END
};

union SwitcherData
{
    uint32_t delay;
    hw::kbd::Package package;
    uint8_t leds;
};

using SwitcherMessage = common::Message<SwitcherCommand, SwitcherData>;

} /* namespace msg */

class SwitcherTask final
  : public common::IKahnProcessTask<config::SwitcherTask::NAME,
                                    msg::SwitcherMessage,
                                    config::SwitcherTask::FIFO_SIZE>
{
 public:
    using Key = hw::kbd::Key;
    using SpecialKey = hw::kbd::SpecialKey;
    using KbdBufferedConverter = utils::kbd::KbdBufferedConverter;

    SwitcherTask() :
      IKahnProcessTask(config::SwitcherTask::STACK_SIZE,
                       config::SwitcherTask::PRIORITY)
    {
        _pastilda_key.set(Key::KEY_GRAVE_ACCENT_AND_TILDE,
                          SpecialKey::LEFT_CTRL);
        this->Start();
    };

    virtual ~SwitcherTask() = default;
    void Run();

 private:
    enum State
    {
        NORMAL,
        PASTILDA,
    };

    enum StateAction
    {
        STAY,
        NEXT,
    };

    State _state = State::NORMAL;
    utils::kbd::KeyObj _pastilda_key;
    utils::kbd::KeyObj _last_key;
    bool _pastilda_key_pressed = false;
    uint8_t _last_led_combination = 0;
    KbdBufferedConverter _kbd_buffered_converter;

    StateAction _normal_state_proc();
    StateAction _pastilda_state_proc();
};

} /* namespace tasks */

#endif /* TASKS_SWITCHERTASK_H_ */
