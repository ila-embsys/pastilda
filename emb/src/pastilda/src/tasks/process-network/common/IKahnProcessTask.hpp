/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_KAHNPROCESS_H_
#define TASKS_KAHNPROCESS_H_

#include <queue.hpp>
#include <thread.hpp>

#include "Fifo.hpp"

namespace rtos = cpp_freertos;

namespace common {

template<const char* U, class T, uint32_t N>
class IKahnProcessTask : public rtos::Thread
{
 public:
    using MsgType = T;
    static constexpr uint32_t FIFO_SIZE = N;
    using FifoType = Fifo<MsgType, FIFO_SIZE>;

    static void fifo_push(MsgType* message) { _input_fifo.Enqueue(message); }

    static void fifo_push_from_isr(MsgType* message)
    {
        _input_fifo.EnqueueFromISR(message, NULL);
    }

    IKahnProcessTask(uint16_t StackDepth, UBaseType_t Priority) :
      Thread(U, StackDepth, Priority){};
    virtual ~IKahnProcessTask() = default;

 protected:
    MsgType _fifo_data;

    void _blocking_read() { _input_fifo.Dequeue(&_fifo_data, portMAX_DELAY); }

    static FifoType _input_fifo;

    virtual void Run() = 0;
};

template<const char* U, class T, uint32_t N>
typename IKahnProcessTask<U, T, N>::FifoType
  IKahnProcessTask<U, T, N>::_input_fifo;

} /* namespace common */

#endif /* TASKS_KAHNPROCESS_H_ */
