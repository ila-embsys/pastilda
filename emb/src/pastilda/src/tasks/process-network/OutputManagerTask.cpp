/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <etl/list.h>
#include <etl/vector.h>
#include <strs/StringView.hpp>
#include <utils/kbd/KeyObj.h>

#include "LogicTask.h"
#include "OutputManagerTask.h"
#include "SwitcherTask.h"

namespace tasks {

// *********************** OUTPUT MANAGER TASK METHODS ***********************

OutputManagerTask::OutputManagerTask() :
  IKahnProcessTask(config::OutputManagerTask::STACK_SIZE,
                   config::OutputManagerTask::PRIORITY),
  _model(_fifo_data)
{
    this->Start();
};

void OutputManagerTask::Run()
{
    while (true) {
        _blocking_read();
        _state_machine_proc();
    }
}

void OutputManagerTask::_state_machine_proc()
{
    StateAction act;
    while (true) {
        switch (_state) {
            case State::IDLE:
                act = _idle_proc();
                if (act == StateAction::NEXT) {
                    _state = State::PASSWORD;
                    return;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::PASSWORD:
                act = _password_proc();
                if (act == StateAction::NEXT) {
                    _state = State::NAVIGATION;
                }
                if (act == StateAction::QUIT) {
                    _state = State::CLEANING;
                }
                if (act == StateAction::FAIL) {
                    _state = State::PASSWORD_FAIL;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::PASSWORD_FAIL:
                act = _password_fail_proc();
                if (act == StateAction::NEXT) {
                    _state = State::IDLE;
                }
                if (act == StateAction::QUIT) {
                    _state = State::CLEANING;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::NAVIGATION:
                act = _navigation_proc();
                if (act == StateAction::QUIT) {
                    _state = State::CLEANING;
                }
                if (act == StateAction::NEXT) {
                    _state = State::LOGIN;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::LOGIN:
                act = _login_proc();
                if (act == StateAction::NEXT) {
                    _state = State::END;
                }
                break;

            case State::CLEANING:
                act = _cleaning_proc();
                if (act == StateAction::NEXT) {
                    _state = State::END;
                }
                break;

            case State::END:
                act = _end_proc();
                if (act == StateAction::NEXT) {
                    _state = State::IDLE;
                }
                break;

            default:
                break;
        }
    }
}

auto OutputManagerTask::_idle_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::PASTILDA_START) {
        _model.update(Model::UpdateType::GREETING_ONLY);
        _printer.print(&_model);

        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto OutputManagerTask::_password_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::END) {
        return StateAction::QUIT;
    }

    if (_fifo_data.token == MsgType::Token::DB_ERROR_MESSAGE) {
        return StateAction::FAIL;
    }

    if (_fifo_data.token == MsgType::Token::MESSAGE) {
        _model.reset();
        _printer.print(&_model);

        return StateAction::NEXT;
    }

    _model.update(Model::UpdateType::APPEND);
    _printer.print(&_model);

    return StateAction::STAY;
}

auto OutputManagerTask::_password_fail_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::PASTILDA_START) {
        return StateAction::NEXT;
    }

    if (_fifo_data.token == MsgType::Token::END) {
        return StateAction::QUIT;
    }

    _model.reset();
    _printer.print(&_model);
    _model.update();
    _printer.print(&_model);

    return StateAction::STAY;
}

auto OutputManagerTask::_navigation_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::CREDENTIALS) {
        _model.reset();
        _printer.print(&_model);

        return StateAction::NEXT;
    }

    if (_fifo_data.token == MsgType::Token::END) {
        return StateAction::QUIT;
    }

    _model.update();
    _printer.print(&_model);

    return STAY;
}

auto OutputManagerTask::_login_proc() -> StateAction
{
    db::Entry& db_entry = *_fifo_data.data.db_entry;

    _printer.print(db_entry);

    LogicTask::MsgType msg;
    msg.token = LogicTask::MsgType::Token::LOGIN_ENDED;
    LogicTask::fifo_push(&msg);

    return NEXT;
}

auto OutputManagerTask::_cleaning_proc() -> StateAction
{
    _model.reset();
    _printer.print(&_model);

    return NEXT;
}

auto OutputManagerTask::_end_proc() -> StateAction
{
    LogicTask::MsgType msg;
    msg.token = LogicTask::MsgType::Token::DISP_ENDED;

    LogicTask::fifo_push(&msg);

    return NEXT;
}

// *********************** PRINTER METHODS ***********************

void OutputManagerTask::Printer::print(const Model* const model)
{
    auto result = _compare_strings(model->main_field, model->main_field_old);

    if (result.is_different) {
        remove(result.remove_length);
        print(result.str_to_print);
    }

    result = _compare_strings(model->greeting_field, model->greeting_field_old);

    if (result.is_different) {
        remove(result.remove_length);
        print(result.str_to_print);
    }
}

void OutputManagerTask::Printer::remove(uint32_t count)
{
    strs::StringFixed<AsciiBufferedConverter::MAX_KEYS_COUNT_PER_PACKAGE> s;

    while (count != 0) {
        count -= 1;
        s.add('\b');

        if (s.full() && count != 0) {
            print(s);
            s.clear();
        }
    }

    if (not s.empty()) {
        print(s);
    }
}

void OutputManagerTask::Printer::print(const char c)
{
    _ascii_buffered_converter.parse(&c, 1);

    SwitcherTask::MsgType msg;
    msg.token = SwitcherTask::MsgType::Token::KEYS_FROM_PASTILDA;
    for (auto& pckg : _kbd_buffer) {
        msg.data.package = pckg;
        SwitcherTask::fifo_push(&msg);
    }

    _kbd_buffer.clear();
}

void OutputManagerTask::Printer::print(const strs::StringView& str)
{
    _ascii_buffered_converter.parse(str);
    for (auto& pckg : _kbd_buffer) {
        SwitcherTask::MsgType msg;
        msg.token = SwitcherTask::MsgType::Token::KEYS_FROM_PASTILDA;
        msg.data.package = pckg;

        SwitcherTask::fifo_push(&msg);
    }

    _kbd_buffer.clear();
}

void OutputManagerTask::Printer::print(db::Entry& db_entry)
{
    if (db_entry.get_sequence().length() == 0) {
        _default_login_print(db_entry);
    }
    else {
        _special_login_print(db_entry);
    }
}

auto OutputManagerTask::Printer::_compare_strings(
  const strs::StringView str_new,
  const strs::StringView str_old) -> CompareResult
{
    CompareResult result;
    auto iter_new = str_new.begin();
    auto iter_old = str_old.begin();

    if (str_new.length() <= str_old.length()) {
        const auto end = str_new.end();
        uint32_t length_diff_part = str_old.length();

        while (iter_new != end && *iter_new == *iter_old) {
            length_diff_part--;
            iter_new++;
            iter_old++;
        }

        result.remove_length = length_diff_part;
        result.str_to_print = strs::StringView(
          iter_new, str_new.length() - (str_old.length() - length_diff_part));
    }
    else if (str_new.length() > str_old.length()) {
        const auto end = str_old.end();
        uint32_t length_diff_part = str_new.length();

        while (iter_old != end && *iter_new == *iter_old) {
            length_diff_part--;
            iter_new++;
            iter_old++;
        }

        result.remove_length =
          str_old.length() - (str_new.length() - length_diff_part);
        result.str_to_print = strs::StringView(iter_new, length_diff_part);
    }

    if (result.str_to_print.length() == 0 && result.remove_length == 0) {
        result.is_different = false;
    }
    else {
        result.is_different = true;
    }

    return result;
}

void OutputManagerTask::Printer::_default_login_print(db::Entry& db_entry)
{
    print(db_entry.get_login());
    print('\t');
    print(db_entry.get_password());
    print('\n');
}

void OutputManagerTask::Printer::_special_login_print(db::Entry& db_entry)
{
    using ReaderResult = AutotypeReader::Result;
    bool is_parse_error = false;
    uint32_t parser_cycle = 0;

    auto autotype_string = db_entry.get_sequence();

    _autotype_reader.set_input(autotype_string);
    ReaderResult result;

    while (result.token != ReaderResult::Token::FINISH && not is_parse_error) {
        parser_cycle++;
        result = _autotype_reader.read();

        switch (result.token) {
            case ReaderResult::Token::STRING:
                print(result.data.str);
                break;

            case ReaderResult::Token::KEY:
                switch (result.data.key) {
                    case ReaderResult::Data::ENTER:
                        print('\n');
                        break;

                    case ReaderResult::Data::TAB:
                        print('\t');
                        break;

                    default:
                        break;
                }
                break;

            case ReaderResult::Token::PASSWORD:
                print(db_entry.get_password());
                break;

            case ReaderResult::Token::USERNAME:
                print(db_entry.get_login());
                break;

            case ReaderResult::Token::DELAY: {
                SwitcherTask::MsgType msg;
                msg.token =
                  SwitcherTask::MsgType::Token::KEYS_PAUSE_FROM_PASTILDA;
                msg.data.delay = result.data.delay;
                SwitcherTask::fifo_push(&msg);
            } break;

            case ReaderResult::Token::NON_VALID:
                if (parser_cycle == 1) {
                    is_parse_error = true;
                }
                break;

            default:
                break;
        }
    }

    if (is_parse_error) {
        _default_login_print(db_entry);
    }
}

// *********************** MODEL METHODS ***********************

void OutputManagerTask::Model::reset()
{
    save();
    greeting_field.reset();
    main_field.reset();
}

void OutputManagerTask::Model::update(UpdateType type)
{
    save();
    switch (type) {
        case GREETING_ONLY:
            reset();
            _update_greeting();
            break;

        case REPLACE:
            main_field.reset();
            _update_main();
            break;

        default:  // APPEND
            _update_main();
            break;
    }
}

void OutputManagerTask::Model::save()
{
    greeting_field_old = greeting_field;
    main_field_old = main_field;
}

void OutputManagerTask::Model::revert()
{
    greeting_field = greeting_field_old;
    main_field = main_field_old;
}

void OutputManagerTask::Model::_update_main()
{
    for (lang::Ascii ascii : _fifo->data.str) {
        if (ascii.code() == lang::Ascii::BACKSPACE) {
            if (not main_field.empty()) {
                main_field.cursor--;
                main_field.pop_back();
            }
        }
        else {
            if (not main_field.full()) {
                main_field.cursor++;
                main_field.add(ascii);
            }
        }
    }
}

void OutputManagerTask::Model::_update_greeting()
{
    greeting_field.set(_fifo->data.str);
    greeting_field.cursor_to_end();
}

template<class T>
auto OutputManagerTask::Model::Field<T>::operator=(const Field& other) -> Field&
{
    this->set(other.data(), other.length());
    this->cursor = other.cursor;

    return *this;
}

template<class T>
void OutputManagerTask::Model::Field<T>::reset()
{
    this->clear();
    cursor_to_begin();
}

} /* namespace tasks */
