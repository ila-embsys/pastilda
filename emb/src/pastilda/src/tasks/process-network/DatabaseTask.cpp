/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <etl/array.h>
#include <hw/UsbDevice.h>
#include <tree/TreeImpl.hpp>
#include <utils/memory/Dict.hpp>
#include <utils/memory/TypedPool.hpp>

#include "DatabaseTask.h"
#include "LogicTask.h"
#include "OutputManagerTask.h"

namespace tasks {

using strs::StringView;
using utils::keepass::KeepassReader;
using namespace utils::memory;
using DbState = _::DbState;

struct _DB_ERRORS : public FlatDict<DbState, StringView, 7>
{
    _DB_ERRORS()
    {
        _add(Key::INVALID_DB_VERSION, Value("!INVALID_DB_VERSION!"));
        _add(Key::INVALID_DB_PASSWORD, Value("!INVALID_DB_PASSWORD!"));
        _add(Key::NO_DB_CREDENTIALS_SELECTED,
             Value("!NO_DB_CREDENTIALS_SELECTED!"));
        _add(Key::CAN_NOT_OPEN_DB_FILE, Value("!CAN NOT OPEN DB.KDBX FILE!"));
        _add(Key::DB_FILE_HASH_IS_CORRUPTED, Value("!DB_FILE_IS_CORRUPTED!"));
        _add(Key::DB_FILE_IS_CLOSED, Value("!DB_FILE_IS_CORRUPTED!"));
        _add(Key::DB_FILE_FORBIDDEN_SYMBOLS,
             Value("!LATIN SYMBOLS ONLY ALLOWED!"));

        set_default(Value("UNKNOWN_ERROR"));
    }
} static db_errors;

inline static void critical_start()
{
    hw::usb::UsbDevice::disable_interrupt();
    rtos::CriticalSection::Enter();
}

inline static void critical_end()
{
    rtos::CriticalSection::Exit();
    hw::usb::UsbDevice::enable_interrupt();
}

/**********************     DATABASE MANAGER CLASS     ***********************/
/********************     DATABASE MANAGER CLASS END     *********************/

/*************************     DECRYPTOR CLASS     **************************/
/***********************     DECRYPTOR CLASS END     ************************/

/**********************     DATABASE TASK METHODS     ***********************/
DatabaseTask::DatabaseTask() :
  IKahnProcessTask(config::DatabaseTask::STACK_SIZE,
                   config::DatabaseTask::PRIORITY),
  _db_manager(_fifo_data)
{
    this->Start();
};

void DatabaseTask::Run()
{
    while (true) {
        _blocking_read();
        _state_machine_proc();
    }
}

void DatabaseTask::_state_machine_proc()
{
    StateAction act;
    while (true) {
        switch (_state) {
            case State::IDLE:
                act = _idle_proc();
                if (act == StateAction::NEXT) {
                    _state = State::DECRYPT;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::DECRYPT:
                act = _decrypt_proc();
                if (act == StateAction::NEXT) {
                    _state = State::OPENED;
                    return;
                }
                if (act == StateAction::FAIL) {
                    _state = State::IDLE;
                    return;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::OPENED:
                act = _opened_proc();
                if (act == StateAction::NEXT) {
                    _state = State::IDLE;
                    return;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;
        }
    }
}

auto DatabaseTask::_idle_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::DECRYPT) {
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto DatabaseTask::_decrypt_proc() -> StateAction
{
    _db_manager.build_database();

    if (not _db_manager.is_database_ok()) {
        _db_manager.close_database();
        _send_fail();
        return StateAction::FAIL;
    }

    LogicTask::MsgType msg;
    msg.token = LogicTask::MsgType::Token::DB_READY;
    LogicTask::fifo_push(&msg);

    return StateAction::NEXT;
}

auto DatabaseTask::_opened_proc() -> StateAction
{
    if (_fifo_data.token == MsgType::Token::DB_CLOSE) {
        _db_manager.close_database();
        return StateAction::NEXT;
    }

    if (_fifo_data.token == MsgType::Token::DECRYPT_PASS) {
        auto& entry = _db_manager.get_current_entry();

        OutputManagerTask::MsgType message;
        message.token = OutputManagerTask::MsgType::Token::CREDENTIALS;
        message.data.db_entry = &entry;
        OutputManagerTask::fifo_push(&message);
    }

    return StateAction::STAY;
}

void DatabaseTask::_send_fail()
{
    OutputManagerTask::MsgType output_msg;
    output_msg.token = OutputManagerTask::MsgType::Token::DB_ERROR_MESSAGE;
    output_msg.data.str =
      db_errors.get_value((DbState)_db_manager.get_database_state());
    OutputManagerTask::fifo_push(&output_msg);

    LogicTask::MsgType notify_msg;
    notify_msg.token = LogicTask::MsgType::Token::DB_ERROR;
    LogicTask::fifo_push(&notify_msg);
}

/*********************     DATABASE MANAGER METHODS    **********************/
DatabaseTask::DatabaseManager::DatabaseManager(const MsgType& fifo) :
  _is_db_ok(true),
  _tree(&Tree::Instance()),
  _decrypter(fifo, std::move(Builder(_tree, &_string_pool)))
{
}

void DatabaseTask::DatabaseManager::build_database()
{
    _is_db_ok = true;

    _decrypter.set_database(config::DatabaseTask::DB_NAME);

    _decrypter.verify_db_password();
    if (_decrypter.is_error()) {
        _is_db_ok = false;
        return;
    }

    _decrypter.decrypt();
    if (_decrypter.is_error()) {
        _is_db_ok = false;
        return;
    }
}

void DatabaseTask::DatabaseManager::close_database()
{
    _tree->destroy();
    _string_pool.release_all();
}

db::Entry& DatabaseTask::DatabaseManager::get_current_entry()
{
    _decrypter.decrypt_entry_password(_tree->begin(),
                                      _tree->get_current_node());

    _decrypted_entry.clear();

    auto password = _decrypter.get_entry_password();
    if (password != nullptr) {
        _decrypted_entry = _tree->get_current_node()->get_container();
        _decrypted_entry.set_password(*password);
    }

    return _decrypted_entry;
}

auto DatabaseTask::DatabaseManager::get_database_state() -> DbState
{
    return static_cast<DbState>(_decrypter.get_db_state());
}

/*************************     DECRYPTER METHODS    **************************/

DatabaseTask::DatabaseManager::Decrypter::Decrypter(const MsgType& fifo,
                                                    Builder&& tree_builder) :
  _db_name("\0"),
  _fifo(&fifo),
  _tree_builder(tree_builder),
  _db_state(DbState::NOT_A_STATE),
  _is_error(false)
{
    _processing_buffer.fill(0);
};

void DatabaseTask::DatabaseManager::Decrypter::verify_db_password()
{
    _is_error = false;
    _set_db_password();

    critical_start();
    _db_state = (DbState)_keepass_reader.check_db_file(_db_name);
    critical_end();
    if (_db_state != DbState::DB_OK) {
        _is_error = true;
        return;
    }

    critical_start();
    _db_state = (DbState)_keepass_reader.check_db_version();
    critical_end();
    if (_db_state != DbState::DB_OK) {
        _is_error = true;
        return;
    }

    critical_start();
    _db_state = (DbState)_keepass_reader.check_db_credentials();
    critical_end();
    if (_db_state != DbState::DB_OK) {
        _is_error = true;
        return;
    }
}

void DatabaseTask::DatabaseManager::Decrypter::decrypt()
{
    if (_is_error) {
        return;
    }

    auto is_db_block_decrypted = [&]() -> bool {
        return _db_state == DbState::NOT_A_STATE || _db_state == DbState::DB_OK;
    };

    critical_start();
    _keepass_reader.read_start();
    critical_end();

    // Process first block
    Builder::XmlSource xml_source;
    xml_source.data = _processing_buffer.data();
    critical_start();
    _db_state = (DbState)_keepass_reader.read_next_chunk(xml_source.data,
                                                         &xml_source.length);
    critical_end();

    if (not is_db_block_decrypted()) {
        _error_process();
        return;
    }

    auto treeState = _tree_builder.build_first_block(xml_source);

    if (treeState == Builder::TreeState::BROKEN) {
        _db_state = DbState::DB_FILE_FORBIDDEN_SYMBOLS;
        _error_process();
    }

    // Processing xml-file block by block
    while (treeState != Builder::TreeState::FINISHED) {
        critical_start();
        _db_state = (DbState)_keepass_reader.read_next_chunk(
          xml_source.data, &xml_source.length);
        critical_end();

        if (not is_db_block_decrypted()) {
            _error_process();
            return;
        }

        treeState = _tree_builder.build_next_block(xml_source);

        if (treeState == Builder::TreeState::BROKEN) {
            _db_state = DbState::DB_FILE_FORBIDDEN_SYMBOLS;
            _error_process();
            return;
        }
    }

    critical_start();
    _keepass_reader.read_stop();
    critical_end();
}

void DatabaseTask::DatabaseManager::Decrypter::decrypt_entry_password(
  Tree::iterator start,
  const Tree::Node* current_node)
{
    if (_is_error) {
        return;
    }

    _keepass_reader.reset_password_decryption();

    auto iter = start;
    auto iter_end = start;

    // Find end iterator
    while (&*iter_end != current_node) {
        iter_end++;
    }
    iter_end++;

    // Decrypt password
    while (iter != iter_end) {
        auto& container = iter->get_container();
        auto& password = container.get_password();

        if (container.is_passw_protected() && password.length() != 0) {
            _entry_password = password;
            _keepass_reader.decrypt_password(&_entry_password);
        }

        iter++;
    }
}

auto DatabaseTask::DatabaseManager::Decrypter::get_entry_password() -> Password*
{
    if (_is_error) {
        return nullptr;
    }

    return &_entry_password;
}

void DatabaseTask::DatabaseManager::Decrypter::_set_db_password()
{
    auto& password = _fifo->data;
    _keepass_reader.set_credentials(password.data(), password.length());
}

void DatabaseTask::DatabaseManager::Decrypter::_error_process()
{
    _is_error = true;
    critical_start();
    _keepass_reader.read_stop();
    critical_end();
}

} /* namespace tasks */
