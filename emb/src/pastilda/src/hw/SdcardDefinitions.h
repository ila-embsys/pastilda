/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_SDCARD_DEFINITIONS_H_
#define HW_SDCARD_DEFINITIONS_H_

namespace hw {

namespace sdcard {

enum class OpResult : uint8_t
{
    OK,
    CMD_CRC_FAIL,
    DATA_CRC_FAIL,
    CMD_RSP_TIMEOUT,
    DATA_TIMEOUT,
    TX_UNDERRUN,
    RX_OVERRUN,
    START_BIT_ERR,
    CMD_OUT_OF_RANGE,
    ADDR_MISALIGNED,
    BLOCK_LEN_ERR,
    ERASE_SEQ_ERR,
    BAD_ERASE_PARAM,
    WRITE_PROT_VIOLATION,
    LOCK_UNLOCK_FAILED,
    COM_CRC_FAILED,
    ILLEGAL_CMD,
    CARD_ECC_FAILED,
    CC_ERROR,
    GENERAL_UNKNOWN_ERROR,
    STREAM_READ_UNDERRUN,
    STREAM_WRITE_OVERRUN,
    CID_CSD_OVERWRITE,
    WP_ERASE_SKIP,
    CARD_ECC_DISABLED,
    ERASE_RESET,
    AKE_SEQ_ERROR,
    INVALID_VOLTRANGE,
    ADDR_OUT_OF_RANGE,
    SWITCH_ERROR,
    SDIO_DISABLED,
    SDIO_FUNCTION_BUSY,
    SDIO_FUNCTION_FAILED,
    SDIO_UNKNOWN_FUNCTION,
    INTERNAL_ERROR,
    NOT_CONFIGURED,
    REQUEST_PENDING,
    REQUEST_NOT_APPLICABLE,
    INVALID_PARAMETER,
    UNSUPPORTED_FEATURE,
    UNSUPPORTED_HW,
    ERROR,
};

enum class TransferState : uint8_t
{
    OK,
    BUSY,
    ERROR
};

enum class CardState : uint32_t
{
    READY = ((uint32_t)0x00000001),
    IDENTIFICATION = ((uint32_t)0x00000002),
    STANDBY = ((uint32_t)0x00000003),
    TRANSFER = ((uint32_t)0x00000004),
    SENDING = ((uint32_t)0x00000005),
    RECEIVING = ((uint32_t)0x00000006),
    PROGRAMMING = ((uint32_t)0x00000007),
    DISCONNECTED = ((uint32_t)0x00000008),
    ERROR = ((uint32_t)0x000000FF),
};

struct CardSpecificData
{
    uint8_t CSDStruct;
    uint8_t SysSpecVersion;
    uint8_t Reserved1;
    uint8_t TAAC;
    uint8_t NSAC;
    uint8_t MaxBusClkFrec;
    uint16_t CardComdClasses;
    uint8_t RdBlockLen;
    uint8_t PartBlockRead;
    uint8_t WrBlockMisalign;
    uint8_t RdBlockMisalign;
    uint8_t DSRImpl;
    uint8_t Reserved2;
    uint32_t DeviceSize;
    uint8_t MaxRdCurrentVDDMin;
    uint8_t MaxRdCurrentVDDMax;
    uint8_t MaxWrCurrentVDDMin;
    uint8_t MaxWrCurrentVDDMax;
    uint8_t DeviceSizeMul;
    uint8_t EraseGrSize;
    uint8_t EraseGrMul;
    uint8_t WrProtectGrSize;
    uint8_t WrProtectGrEnable;
    uint8_t ManDeflECC;
    uint8_t WrSpeedFact;
    uint8_t MaxWrBlockLen;
    uint8_t WriteBlockPaPartial;
    uint8_t Reserved3;
    uint8_t ContentProtectAppli;
    uint8_t FileFormatGrouop;
    uint8_t CopyFlag;
    uint8_t PermWrProtect;
    uint8_t TempWrProtect;
    uint8_t FileFormat;
    uint8_t ECC;
    uint8_t CSD_CRC;
    uint8_t Reserved4;
};

struct CardIDentificationNumber
{
    uint8_t ManufacturerID;
    uint16_t OEM_AppliID;
    uint32_t ProdName1;
    uint8_t ProdName2;
    uint8_t ProdRev;
    uint32_t ProdSN;
    uint8_t Reserved1;
    uint16_t ManufactDate;
    uint8_t CID_CRC;
    uint8_t Reserved2;
};

/**
 * SD status register
 *
 * See RM0090 Rev 18, p. 1045
 */
struct SdStatusReg
{
    uint8_t DAT_BUS_WIDTH;
    uint8_t SECURED_MODE;
    uint16_t SD_CARD_TYPE;
    uint32_t SIZE_OF_PROTECTED_AREA;
    uint8_t SPEED_CLASS;
    uint8_t PERFORMANCE_MOVE;
    uint8_t AU_SIZE;
    uint16_t ERASE_SIZE;
    uint8_t ERASE_TIMEOUT;
    uint8_t ERASE_OFFSET;
};


/**
 * SD status register
 *
 * See RM0090 Rev 18, p. 1045
 */
struct [[gnu::packed]] SdStatusReg_
{
    /// 2 bit field in struct
    enum class DataBusWidth : uint16_t
    {
        _1 = 0b00,
        _4 = 0b10
    };

    /// 1 bit field in struct
    enum class SecuredMode : uint16_t
    {
        ENABLED = 0b1,
        DISABLED = 0b0
    };

    enum class SdCardType : uint16_t
    {
        REGULAR_SD_RD_RW = 0x0000,
        SD_CROM_CARD = 0x0001,
    };

    enum class SpeedClass : uint8_t
    {
        CLASS_0 = 0x00,
        CLASS_2 = 0x01,
        CLASS_4 = 0x02,
        CLASS_6 = 0x03,
        // 0x04-0xFF - reserved
    };

    /// 4 bit field in struct
    enum class AuSize : uint16_t
    {
        NOT_DEFINED = 0x00,
        _16KB = 0x01,
        _32KB = 0x02,
        _64KB = 0x03,
        _128KB = 0x04,
        _256KB = 0x05,
        _512KB = 0x06,
        _1MB = 0x07,
        _2MB = 0x08,
        _4MB = 0x09,
        // 0x0A-0x10 - reserved
    };

    /// 2 bit field in struct
    enum class EraseOffset : uint16_t
    {
        _0_SEC = 0x0,
        _1_SEC = 0x1,
        _2_SEC = 0x2,
        _3_SEC = 0x3
    };

    // GCC warning "is too small to hold all values of enum class"
    // is OK. Unfortunately, GCC has not ability to suppress them
    // by pragmas.

    // clang-format off
    public: DataBusWidth data_bus_width : 2;
    public: SecuredMode secured_mode : 1;
    private: uint16_t reserved_508_496 : 13;
    public: SdCardType card_type;
    public: uint32_t size_of_protec_area;
    public: SpeedClass speed_class;
    public: uint8_t performance_move;  // 0 - not defined, 1-254 - Mb/s, 255 - infinity
    public: AuSize au_size : 4;
    private: uint16_t reserved_427_424 : 4;
    public: uint16_t erase_size;  // 0 - not supported, 0x1-0xffff - count of AUs to erase
    public: uint16_t erase_timeout : 6;  // 0 - not supported, 1-63 - sec
    public: EraseOffset erase_offset : 2;
    private: uint8_t reserved_399_312[11];
    public: uint8_t reserved_for_manufacturer[39];
    // clang-format on
};

static_assert(sizeof(SdStatusReg_) == 64,
              "Size of SD status register should be 512 bit!");


struct CardInfo
{
    CardSpecificData csd;
    CardIDentificationNumber cid;
    uint64_t capacity;
    uint32_t block_size;
    uint16_t rca;  //!< relative card address
    uint8_t card_type;
};

constexpr uint8_t SD_CMD_GO_IDLE_STATE = 0;
constexpr uint8_t SD_CMD_SEND_OP_COND = 1;
constexpr uint8_t SD_CMD_ALL_SEND_CID = 2;
constexpr uint8_t SD_CMD_SET_REL_ADDR = 3;
constexpr uint8_t SD_CMD_SET_DSR = 4;
constexpr uint8_t SD_CMD_SDIO_SEN_OP_COND = 5;
constexpr uint8_t SD_CMD_HS_SWITCH = 6;
constexpr uint8_t SD_CMD_SEL_DESEL_CARD = 7;
constexpr uint8_t SD_CMD_HS_SEND_EXT_CSD = 8;
constexpr uint8_t SD_CMD_SEND_CSD = 9;
constexpr uint8_t SD_CMD_SEND_CID = 10;
constexpr uint8_t SD_CMD_READ_DAT_UNTIL_STOP = 11;
constexpr uint8_t SD_CMD_STOP_TRANSMISSION = 12;
constexpr uint8_t SD_CMD_SEND_STATUS = 13;
constexpr uint8_t SD_CMD_HS_BUSTEST_READ = 14;
constexpr uint8_t SD_CMD_GO_INACTIVE_STATE = 15;
constexpr uint8_t SD_CMD_SET_BLOCKLEN = 16;
constexpr uint8_t SD_CMD_READ_SINGLE_BLOCK = 17;
constexpr uint8_t SD_CMD_READ_MULT_BLOCK = 18;
constexpr uint8_t SD_CMD_HS_BUSTEST_WRITE = 19;
constexpr uint8_t SD_CMD_WRITE_DAT_UNTIL_STOP = 20;
constexpr uint8_t SD_CMD_SET_BLOCK_COUNT = 23;
constexpr uint8_t SD_CMD_WRITE_SINGLE_BLOCK = 24;
constexpr uint8_t SD_CMD_WRITE_MULT_BLOCK = 25;
constexpr uint8_t SD_CMD_PROG_CID = 26;
constexpr uint8_t SD_CMD_PROG_CSD = 27;
constexpr uint8_t SD_CMD_SET_WRITE_PROT = 28;
constexpr uint8_t SD_CMD_CLR_WRITE_PROT = 29;
constexpr uint8_t SD_CMD_SEND_WRITE_PROT = 30;
constexpr uint8_t SD_CMD_SD_ERASE_GRP_START = 32;
constexpr uint8_t SD_CMD_SD_ERASE_GRP_END = 33;
constexpr uint8_t SD_CMD_ERASE_GRP_START = 35;
constexpr uint8_t SD_CMD_ERASE_GRP_END = 36;
constexpr uint8_t SD_CMD_ERASE = 38;
constexpr uint8_t SD_CMD_FAST_IO = 39;
constexpr uint8_t SD_CMD_GO_IRQ_STATE = 40;
constexpr uint8_t SD_CMD_LOCK_UNLOCK = 42;
constexpr uint8_t SD_CMD_APP_CMD = 55;
constexpr uint8_t SD_CMD_GEN_CMD = 56;
constexpr uint8_t SD_CMD_NO_CMD = 64;

constexpr uint8_t SD_CMD_APP_SD_SET_BUSWIDTH = 6;
constexpr uint8_t SD_CMD_SD_APP_STAUS = 13;
constexpr uint8_t SD_CMD_SD_APP_SEND_NUM_WRITE_BLOCKS = 22;
constexpr uint8_t SD_CMD_SD_APP_OP_COND = 41;
constexpr uint8_t SD_CMD_SD_APP_SET_CLR_CARD_DETECT = 42;
constexpr uint8_t SD_CMD_SD_APP_SEND_SCR = 51;
constexpr uint8_t SD_CMD_SDIO_RW_DIRECT = 52;
constexpr uint8_t SD_CMD_SDIO_RW_EXTENDED = 53;

constexpr uint8_t SD_CMD_SD_APP_GET_MKB = 43;
constexpr uint8_t SD_CMD_SD_APP_GET_MID = 44;
constexpr uint8_t SD_CMD_SD_APP_SET_CER_RN1 = 45;
constexpr uint8_t SD_CMD_SD_APP_GET_CER_RN2 = 46;
constexpr uint8_t SD_CMD_SD_APP_SET_CER_RES2 = 47;
constexpr uint8_t SD_CMD_SD_APP_GET_CER_RES1 = 48;
constexpr uint8_t SD_CMD_SD_APP_SECURE_READ_MULTIPLE_BLOCK = 18;
constexpr uint8_t SD_CMD_SD_APP_SECURE_WRITE_MULTIPLE_BLOCK = 25;
constexpr uint8_t SD_CMD_SD_APP_SECURE_ERASE = 38;
constexpr uint8_t SD_CMD_SD_APP_CHANGE_SECURE_AREA = 49;
constexpr uint8_t SD_CMD_SD_APP_SECURE_WRITE_MKB = 48;

constexpr uint8_t SD_PRESENT = 0x01;
constexpr uint8_t SD_NOT_PRESENT = 0x00;

constexpr uint32_t SDIO_STD_CAPACITY_SD_CARD_V1_1 = 0x00000000;
constexpr uint32_t SDIO_STD_CAPACITY_SD_CARD_V2_0 = 0x00000001;
constexpr uint32_t SDIO_HIGH_CAPACITY_SD_CARD = 0x00000002;
constexpr uint32_t SDIO_MULTIMEDIA_CARD = 0x00000003;
constexpr uint32_t SDIO_SECURE_DIGITAL_IO_CARD = 0x00000004;
constexpr uint32_t SDIO_HIGH_SPEED_MULTIMEDIA_CARD = 0x00000005;
constexpr uint32_t SDIO_SECURE_DIGITAL_IO_COMBO_CARD = 0x00000006;
constexpr uint32_t SDIO_HIGH_CAPACITY_MMC_CARD = 0x00000007;

constexpr uint32_t SDIO_STATIC_FLAGS = 0x000005FF;
constexpr uint32_t SDIO_CMD0TIMEOUT = 0x00010000;

constexpr uint32_t SD_OCR_ADDR_OUT_OF_RANGE = 0x80000000;
constexpr uint32_t SD_OCR_ADDR_MISALIGNED = 0x40000000;
constexpr uint32_t SD_OCR_BLOCK_LEN_ERR = 0x20000000;
constexpr uint32_t SD_OCR_ERASE_SEQ_ERR = 0x10000000;
constexpr uint32_t SD_OCR_BAD_ERASE_PARAM = 0x08000000;
constexpr uint32_t SD_OCR_WRITE_PROT_VIOLATION = 0x04000000;
constexpr uint32_t SD_OCR_LOCK_UNLOCK_FAILED = 0x01000000;
constexpr uint32_t SD_OCR_COM_CRC_FAILED = 0x00800000;
constexpr uint32_t SD_OCR_ILLEGAL_CMD = 0x00400000;
constexpr uint32_t SD_OCR_CARD_ECC_FAILED = 0x00200000;
constexpr uint32_t SD_OCR_CC_ERROR = 0x00100000;
constexpr uint32_t SD_OCR_GENERAL_UNKNOWN_ERROR = 0x00080000;
constexpr uint32_t SD_OCR_STREAM_READ_UNDERRUN = 0x00040000;
constexpr uint32_t SD_OCR_STREAM_WRITE_OVERRUN = 0x00020000;
constexpr uint32_t SD_OCR_CID_CSD_OVERWRIETE = 0x00010000;
constexpr uint32_t SD_OCR_WP_ERASE_SKIP = 0x00008000;
constexpr uint32_t SD_OCR_CARD_ECC_DISABLED = 0x00004000;
constexpr uint32_t SD_OCR_ERASE_RESET = 0x00002000;
constexpr uint32_t SD_OCR_AKE_SEQ_ERROR = 0x00000008;
constexpr uint32_t SD_OCR_ERRORBITS = 0xFDFFE008;

constexpr uint32_t SD_R6_GENERAL_UNKNOWN_ERROR = 0x00002000;
constexpr uint32_t SD_R6_ILLEGAL_CMD = 0x00004000;
constexpr uint32_t SD_R6_COM_CRC_FAILED = 0x00008000;

constexpr uint32_t SD_VOLTAGE_WINDOW_SD = 0x80100000;
constexpr uint32_t SD_HIGH_CAPACITY = 0x40000000;
constexpr uint32_t SD_STD_CAPACITY = 0x00000000;
constexpr uint32_t SD_CHECK_PATTERN = 0x000001AA;

constexpr uint32_t SD_MAX_VOLT_TRIAL = 0x0000FFFF;
constexpr uint32_t SD_ALLZERO = 0x00000000;

constexpr uint32_t SD_WIDE_BUS_SUPPORT = 0x00040000;
constexpr uint32_t SD_SINGLE_BUS_SUPPORT = 0x00010000;
constexpr uint32_t SD_CARD_LOCKED = 0x02000000;

constexpr uint32_t SD_DATATIMEOUT = 0xFFFFFFFF;
constexpr uint32_t SD_0TO7BITS = 0x000000FF;
constexpr uint32_t SD_8TO15BITS = 0x0000FF00;
constexpr uint32_t SD_16TO23BITS = 0x00FF0000;
constexpr uint32_t SD_24TO31BITS = 0xFF000000;
constexpr uint32_t SD_MAX_DATA_LENGTH = 0x01FFFFFF;

constexpr uint32_t SD_HALFFIFO = 0x00000008;
constexpr uint32_t SD_HALFFIFOBYTES = 0x00000020;

constexpr uint32_t SD_CCCC_LOCK_UNLOCK = 0x00000080;
constexpr uint32_t SD_CCCC_WRITE_PROT = 0x00000040;
constexpr uint32_t SD_CCCC_ERASE = 0x00000020;

constexpr uint32_t SDIO_SEND_IF_COND = 0x00000008;

enum class BusClockDiv : uint8_t
{
    INIT = 118,
    TRANSFER = 0
};

constexpr uint32_t BLOCK_SIZE = 512;

} /* namespace sdcard */

} /* namespace hw */

#endif /* HW_SDCARD_H_ */
