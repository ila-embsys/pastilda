/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_USBHOST_H_
#define HW_USBHOST_H_

#include <cm3cpp/timer.hpp>
#include <config/HwConfig.h>

BEGIN_DECLS
#include <libusbhost/usbh_core.h>
#include <libusbhost/usbh_driver_hid_kbd.h>
#include <libusbhost/usbh_driver_hub.h>
#include <libusbhost/usbh_lld_stm32f4.h>
END_DECLS

namespace hw {

namespace usb {

typedef void (*message_handler)(uint8_t device_id,
                                const uint8_t* data,
                                uint32_t length);

class UsbHost
{
 public:
    UsbHost(message_handler handler);
    ~UsbHost() = default;

    uint32_t get_time_us();
    void start();

    static constexpr usbh_dev_driver_t* device_drivers[] = {
      (usbh_dev_driver_t*)&usbh_hid_kbd_driver,
      (usbh_dev_driver_t*)&usbh_hub_driver, 0};

    static constexpr void* lld_drivers[] = {(void*)&driver_hs, 0};

 private:
    cm3cpp::tim::Timer _timer;
};

} /* namespace usb */

} /* namespace hw */

#endif /* HW_USBHOST_H_ */
