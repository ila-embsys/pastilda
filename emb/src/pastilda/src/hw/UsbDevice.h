/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_USBDEVICE_H_
#define HW_USBDEVICE_H_

#include <array>

#include <config/HwConfig.h>

BEGIN_DECLS
#include <libopencm3/usb/usbd.h>  // should be before msc.h

#include <libopencm3/usb/hid.h>
#include <libopencm3/usb/msc.h>
END_DECLS

#include "FileSystem.h"

namespace hw {

namespace usb {

constexpr uint32_t USB_DEVICE_BUFFER_SIZE = 500;

typedef enum
{
    I_KEYBOARD = 0,
    I_MASS_STORAGE = 1
} Interface;

typedef enum
  : uint8_t
{
    E_KEYBOARD = 0x81,
    E_MASS_STORAGE_IN = 0x83,
    E_MASS_STORAGE_OUT = 0x03
} Endpoint;

typedef enum
{
    GET_REPORT = 1,
    GET_IDLE = 2,
    GET_PROTOCOL = 3,
    SET_REPORT = 9,
    SET_IDLE = 10,
    SET_PROTOCOL = 11,
} HidRequest;

class UsbDevice
{
 public:
    static constexpr uint32_t REMOTE_WAKEUP_DELAY_MS = 10;
    static constexpr uint8_t keyboard_report_descriptor[] = {
      0x05, 0x01, 0x09, 0x06, 0xA1, 0x01, 0x05, 0x07, 0x19, 0xE0, 0x29, 0xE7, 0x15,
      0x00, 0x25, 0x01, 0x75, 0x01, 0x95, 0x08, 0x81, 0x02, 0x95, 0x01, 0x75, 0x08,
      0x81, 0x01, 0x95, 0x03, 0x75, 0x01, 0x05, 0x08, 0x19, 0x01, 0x29, 0x03, 0x91,
      0x02, 0x95, 0x05, 0x75, 0x01, 0x91, 0x01, 0x95, 0x06, 0x75, 0x08, 0x15, 0x00,
      0x26, 0xFF, 0x00, 0x05, 0x07, 0x19, 0x00, 0x2A, 0xFF, 0x00, 0x81, 0x00, 0xC0};

    static constexpr std::array usb_strings = {
      /* iManufacturer description */
      "Third pin LLC",
      /* iProduct description */
      "Pastilda Composite Device",
      /* iSerialNumber description */
      "Pastilda",

      /* iConfiguration description */
      "Main configuration",

      /* iInterface description */
      "HID interface - Pastilda",
    };

    static constexpr struct usb_device_descriptor dev = {.bLength = USB_DT_DEVICE_SIZE,
                                                         .bDescriptorType = USB_DT_DEVICE,
                                                         .bcdUSB = 0x0110,
                                                         .bDeviceClass = 0x0,
                                                         .bDeviceSubClass = 0x00,
                                                         .bDeviceProtocol = 0x00,
                                                         .bMaxPacketSize0 = 64,
                                                         .idVendor = 0x0483,
                                                         .idProduct = 0x5741,
                                                         .bcdDevice = 0x0200,
                                                         .iManufacturer = 1,
                                                         .iProduct = 2,
                                                         .iSerialNumber = 3,
                                                         .bNumConfigurations = 1};

    struct [[gnu::packed]] type_hid_function
    {
        usb_hid_descriptor hid_descriptor;

        struct [[gnu::packed]]
        {
            uint8_t bReportDescriptorType;
            uint16_t wDescriptorLength;
        }
        hid_report;
    };

    static constexpr type_hid_function keyboard_hid_function = {
      .hid_descriptor = {.bLength = 9,
                         .bDescriptorType = USB_DT_HID,
                         .bcdHID = 0x0111,
                         .bCountryCode = 0,
                         .bNumDescriptors = 1},
      .hid_report = {.bReportDescriptorType = USB_DT_REPORT,
                     .wDescriptorLength = sizeof(keyboard_report_descriptor)}};

    static constexpr struct usb_endpoint_descriptor hid_endpoint = {
      .bLength = USB_DT_ENDPOINT_SIZE,
      .bDescriptorType = USB_DT_ENDPOINT,
      .bEndpointAddress = Endpoint::E_KEYBOARD,
      .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
      .wMaxPacketSize = 64,
      .bInterval = 0x5};

    static constexpr struct usb_endpoint_descriptor msc_endpoint[] = {
      {.bLength = USB_DT_ENDPOINT_SIZE,
       .bDescriptorType = USB_DT_ENDPOINT,
       .bEndpointAddress = Endpoint::E_MASS_STORAGE_IN,
       .bmAttributes = USB_ENDPOINT_ATTR_BULK,
       .wMaxPacketSize = 64,
       .bInterval = 0},
      {.bLength = USB_DT_ENDPOINT_SIZE,
       .bDescriptorType = USB_DT_ENDPOINT,
       .bEndpointAddress = Endpoint::E_MASS_STORAGE_OUT,
       .bmAttributes = USB_ENDPOINT_ATTR_BULK,
       .wMaxPacketSize = 64,
       .bInterval = 0}};

    static constexpr struct usb_interface_descriptor iface[] = {
      {.bLength = USB_DT_INTERFACE_SIZE,
       .bDescriptorType = USB_DT_INTERFACE,
       .bInterfaceNumber = Interface::I_KEYBOARD,
       .bAlternateSetting = 0,
       .bNumEndpoints = 1,
       .bInterfaceClass = USB_CLASS_HID,
       .bInterfaceSubClass = 1,
       .bInterfaceProtocol = 1,
       .iInterface = 0,

       .endpoint = const_cast<usb_endpoint_descriptor*>(&hid_endpoint),
       .extra = (void*)&keyboard_hid_function,
       .extralen = sizeof(keyboard_hid_function)},

      {.bLength = USB_DT_INTERFACE_SIZE,
       .bDescriptorType = USB_DT_INTERFACE,
       .bInterfaceNumber = Interface::I_MASS_STORAGE,
       .bAlternateSetting = 0,
       .bNumEndpoints = 2,
       .bInterfaceClass = USB_CLASS_MSC,
       .bInterfaceSubClass = USB_MSC_SUBCLASS_SCSI,
       .bInterfaceProtocol = USB_MSC_PROTOCOL_BBB,
       .iInterface = 0x00,

       .endpoint = const_cast<usb_endpoint_descriptor*>(msc_endpoint),
       .extra = nullptr,
       .extralen = 0},
    };

    static constexpr struct usb_interface ifaces[]{
      {.cur_altsetting = nullptr,
       .num_altsetting = 1,
       .iface_assoc = nullptr,
       .altsetting = &iface[Interface::I_KEYBOARD]},

      {.cur_altsetting = nullptr,
       .num_altsetting = 1,
       .iface_assoc = nullptr,
       .altsetting = &iface[Interface::I_MASS_STORAGE]},
    };

    // FIXME: [USB] attributes is a struct of bits
    static constexpr struct usb_config_descriptor config_descr = {
      .bLength = USB_DT_CONFIGURATION_SIZE,
      .bDescriptorType = USB_DT_CONFIGURATION,
      .wTotalLength = 0,
      .bNumInterfaces = 2,
      .bConfigurationValue = 1,
      .iConfiguration = 0,
      .bmAttributes = 0xA0,
      .bMaxPower = 80,
      .interface = ifaces};

    using IsCbRegistred = bool;

    UsbDevice();
    ~UsbDevice() = default;

    void poll() { usbd_poll(_device); }

    void msd_init();

    static void enable_interrupt();
    static void disable_interrupt();

    bool is_host_suspend(void);
    void remote_wakeup(bool on);

    uint16_t ep_write_packet(uint8_t addr, const void* buf, uint16_t len)
    {
        return ::usbd_ep_write_packet(_device, addr, buf, len);
    }

    IsCbRegistred register_set_config_callback(usbd_set_config_callback callback)
    {
        return ::usbd_register_set_config_callback(_device, callback) == 0;
    }

 private:
    std::array<uint8_t, USB_DEVICE_BUFFER_SIZE> _buffer;
    usbd_device* _device;
};

} /* namespace usb */

} /* namespace hw */

#endif /* HW_USBDEVICE_H_ */
