/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_KBD_PACKAGE_HPP_
#define HW_KBD_PACKAGE_HPP_

#include <cstddef>
#include <cstring>

#include "hw/kbd/KeyCodes.hpp"

using std::memset;
using std::size_t;

namespace hw {

namespace kbd {

constexpr static uint8_t PACKAGE_EMPTY_FIELD = 0;

constexpr static size_t PACKAGE_LENGTH = 8;
constexpr static size_t PACKAGE_SPECIAL_FIELDS_LENGTH = 2;
constexpr static size_t PACKAGE_KEY_FIELDS_LENGTH = 6;

#pragma pack(push, 1)
struct Package
{
    SpecialKeySequence special;
    uint8_t reserved;
    Key keys[PACKAGE_KEY_FIELDS_LENGTH];

    friend bool operator==(const Package& lhs, const Package& rhs)
    {
        return (memcmp(&lhs, &rhs, PACKAGE_LENGTH) == 0);
    }

    friend bool operator!=(const Package& lhs, const Package& rhs)
    {
        return !(memcmp(&lhs, &rhs, PACKAGE_LENGTH) == 0);
    }

    uint8_t* data() { return reinterpret_cast<uint8_t*>(this); }

    const uint8_t* data() const
    {
        return reinterpret_cast<const uint8_t*>(this);
    }

    constexpr size_t length() const { return PACKAGE_LENGTH; }

    void clear() { memset(data(), PACKAGE_EMPTY_FIELD, length()); }

    size_t keys_count() const
    {
        int n = PACKAGE_KEY_FIELDS_LENGTH - 1;
        while (keys[n] == Key::NOT_A_KEY && n != -1) {
            --n;
        }

        return n + 1;
    }
};
#pragma pack(pop)

static const Package ZERO_PACKAGE = {
  {PACKAGE_EMPTY_FIELD}, PACKAGE_EMPTY_FIELD, Key::NOT_A_KEY, Key::NOT_A_KEY,
  Key::NOT_A_KEY,        Key::NOT_A_KEY,      Key::NOT_A_KEY, Key::NOT_A_KEY};

}  // namespace kbd

}  // namespace hw

#endif /* HW_KBD_PACKAGE_HPP_ */
