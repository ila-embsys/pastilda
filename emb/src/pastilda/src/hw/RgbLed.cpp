/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RgbLed.h"

namespace hw {

RgbLed::RgbLed() : _leds{config::LED_RED, config::LED_GREEN, config::LED_BLUE}
{
}

void RgbLed::on(Color color)
{
    uint8_t mask = static_cast<uint8_t>(color);

    for (int i = 0; i < LEDS_COUNT; i++) {
        if ((mask & 0b001) == 0b001) {
            _leds[i].on();
        }
        else {
            _leds[i].off();
        }

        mask = mask >> 1;
    }
}

void RgbLed::off()
{
    for (int i = 0; i < LEDS_COUNT; ++i) {
        _leds[i].off();
    }
}

void RgbLed::set_start_color(Color color)
{
    _current_color = color;
}

void RgbLed::circle()
{
    off();
    on(_current_color);

    uint8_t color_index = static_cast<uint8_t>(_current_color) + 1;
    if (color_index > COLORS_COUNT) {
        color_index = 1;
    }

    _current_color = static_cast<Color>(color_index);
}

} /* namespace hw */
