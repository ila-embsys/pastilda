cmake_minimum_required(VERSION 3.10)

add_library(hw INTERFACE)

target_sources(hw INTERFACE
	${CMAKE_CURRENT_LIST_DIR}/FileSystem.cpp
	${CMAKE_CURRENT_LIST_DIR}/RgbLed.cpp
	${CMAKE_CURRENT_LIST_DIR}/SdcardDriver.cpp
	${CMAKE_CURRENT_LIST_DIR}/UsbDevice.cpp
	${CMAKE_CURRENT_LIST_DIR}/UsbHost.cpp
)