/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DATABASE_ENTRY_H_
#define DATABASE_ENTRY_H_

#include <cstddef>
#include <cstdint>
#include <limits>

#include <strs/StringView.hpp>

using std::size_t;

namespace db {

using StringField = strs::StringView;
using StringFieldChar = StringField::value_type;

constexpr const StringFieldChar* EMPTY_FIELD_CHAR = "";

const static StringField EMPTY_FIELD(EMPTY_FIELD_CHAR);

namespace detail {
template<typename T>
static void assert_is_char_like(T var)
{
    using CharLikeType = typename std::remove_pointer<T>::type;

    static_assert(sizeof(CharLikeType) == sizeof(char),
                  "Char-like type should be here!");
}
}  // namespace detail

namespace _ = detail;

class Entry
{
 public:
    Entry();
    Entry(size_t index);
    ~Entry();

    Entry(const Entry& entry);

    void set_index(uint32_t index);

    template<typename T>
    void set_name(T name, size_t length);
    template<typename T>
    void set_login(T login, size_t length);
    template<typename T>
    void set_password(T password, size_t length);
    template<typename T>
    void set_sequence(T type, size_t length);

    void set_name(StringField name) { _name = name; }

    void set_login(StringField login) { _login = login; }

    void set_password(StringField passw) { _password = passw; }

    void set_sequence(StringField seq) { _sequence = seq; }

    void set_password_protection(bool isProtected);

    uint32_t get_index() const { return _index; }

    const StringField& get_login() const { return _login; }

    const StringField& get_name() const { return _name; }

    const StringField& get_password() const { return _password; }

    const StringField& get_sequence() const { return _sequence; }

    bool is_passw_protected() const { return _password_protected; }

    void clear()
    {
        _index = 0;
        _name = EMPTY_FIELD;
        _login = EMPTY_FIELD;
        _password = EMPTY_FIELD;
        _sequence = EMPTY_FIELD;
        _password_protected = false;
    }

 private:
    uint32_t _index;
    StringField _name;
    StringField _login;
    StringField _password;
    StringField _sequence;

    bool _password_protected;
};

// Template functions realization
template<typename T>
void Entry::set_name(T name, size_t length)
{
    _::assert_is_char_like(name);

    auto aName = reinterpret_cast<const StringFieldChar*>(name);

    _name = StringField(aName, length);
}

template<typename T>
void Entry::set_login(T login, size_t length)
{
    _::assert_is_char_like(login);

    auto aLogin = reinterpret_cast<const StringFieldChar*>(login);

    _login = StringField(aLogin, length);
}

template<typename T>
void Entry::set_password(T password, size_t length)
{
    _::assert_is_char_like(password);

    auto aPassw = reinterpret_cast<const StringFieldChar*>(password);

    _password = StringField(aPassw, length);
}

template<typename T>
void Entry::set_sequence(T seq, size_t length)
{
    _::assert_is_char_like(seq);

    auto aSeq = reinterpret_cast<const StringFieldChar*>(seq);

    _sequence = StringField(aSeq, length);
}

} /* namespace db */

#endif /* DATABASE_ENTRY_H_ */
