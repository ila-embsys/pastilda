1. pastilda.bat          -> run it to load bootloader to your Pastilda.

2. boot.hex              -> The latest release of a bootloader (you should
                            place it here, see in release folder).


How to Load New Firmware Using DFU
----------------------------------

If your device have no pre-loaded bootloader you have to go through the next steps:
  * Install driver from "Driver" folder
  * Copy files from dfu/sdcard folder to your SD card and insert it to the Pastilda.
  * Connect Boot0 pin with VDD.
  * Insert your Pastilda to the PC. After that you will see new 'STM Device in DFU Mode' in the Device Manager.
  * After that you can disconnect Boot0 and VDD pins.
  * Run pastilda.bat from dfu folder. It will load bootloader to your device.
  * Once bootloader is loaded it will find pastilda.srec file on your SD card and will start to load firmware.
  * Programming is finished when LEDs on your Pastilda start to blink with 1s period.
  * New Keyboard and Mass Storage Device will appear in the Device Manager.
  * That's it! Now you can connect your keyboard to Pastilda and try it's functionality.