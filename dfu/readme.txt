1. /sdcard               -> files from this folder must be copied to Pastilda's SD card.

2. /sdcard/pastilda.srec -> The latest release of a main program (put it by yourself). 

  * If you want to load specific release from ../release folder or your own firmware just replase this file with desired one. 
  * Our bootloader works just with .srec files!

How to Load New Firmware Using DFU
----------------------------------

For linux you can find an instructions in linux folder.
For windows you can find an instructions in windows folder.


How to Update Firmware
----------------------

If you want to load updated firmware to your device:
  * We will load each new firmware to the release folder. 
  * For updating your device you will need just copy the latest pastilda.srec file to the Pastilda.
  * Then unplug and plug your device. After that the bootloader will start to work.
  * Programming is finished when LEDs on your Pastilda start to blink with 1s period. 
  * Notice that pastilda.srec file on your Pastilda has dissapeared.
  * That's it! Now you can connect your keyboard to Pastilda and try it's new functionality.


What LEDs blinking on My Pastilda Means?!
-----------------------------------------

Now watch the LED on the board. It must go through the next states (you can also watch a video here: https://youtu.be/eg9DWGGIzso):
1. Green flash at a start
2. Blue light blinking
3. Blue light (no blinking)
4. Blue light blinking
5. The colors starts to change each other with 1 second period, a new disk appears at your PC file system. It means, that the programming is finished. This is good Pastilda's behavior.


Play With Our Pre-Loaded Database!
----------------------------------

If you are using Pastilda for the first time start with our sample database db.kdbx located in dfu/sdcard folder:
  1. Copy this file to your Pastilda.
  2. Open Notepad and press Ctrl + ~. Now your keyboard will work in 'Pastilda Mode'.
  3. You will see Pastilda's invitation: >>>
  4. Now you have to enter the password (QWErty for db.kdbx) and press Enter.
  5. After that Pastilda will decrypt the database and you can start to navigate trough it with arrows left, right, up and down. To leave 'Pastilda Mode' just press Esc.
  6. In our pre-loaded database there are just two entries: openbit and wikipedia.
  7. Once you choose resource you want to log in (openbit or wikipedia) just press Enter.
  8. OK! This was is a little demonstration. Now you can try it to really log in somewhere.
  9. Go to ideone.com or wikipedia.org and repeat steps 1 to 7.
  10. That's it! Now you can replace demo db.kdbx file with your own database.
